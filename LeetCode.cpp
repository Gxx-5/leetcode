//header files
#include <iostream>
#include <algorithm> // std::sort
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <vector>
#include <string>
#include <stack>
#include <array>
#include <set>
#include <random>
#include <time.h>

using namespace std;

class LinkedNode
{
public:
	int key, value;
	LinkedNode *prev;
	LinkedNode *next;
	LinkedNode() : key(0), value(0), prev(nullptr), next(nullptr) {}
	LinkedNode(int _key, int _value) : key(_key), value(_value), prev(nullptr), next(nullptr) {}
};
class LRUCache
{
	int _capacity;
	int _size = 0;
	unordered_map<int, LinkedNode *> hashT;
	LinkedNode *head;
	LinkedNode *tail;

public:
	LRUCache(int capacity)
	{
		_capacity = capacity;
		initialLinkNode();
	}

	int get(int key)
	{
		if (!hashT.count(key))
		{
			return -1;
		}
		LinkedNode *p = hashT[key];
		moveToHead(p);
		return p->value;
	}

	void put(int key, int value)
	{
		if (hashT.count(key))
		{
			LinkedNode *p = hashT[key];
			p->value = value;
			moveToHead(p);
			return;
		}
		LinkedNode *p = new LinkedNode(key, value);
		addToHead(p);
		hashT.emplace(key, p);
		if (_size > _capacity)
		{
			hashT.erase(tail->prev->key);
			LinkedNode *removed = removeNode(tail->prev);
			delete removed;
		}
		return;
	}

	void initialLinkNode()
	{
		head = new LinkedNode();
		tail = new LinkedNode();
		head->next = tail;
		tail->prev = head;
	}

	void moveToHead(LinkedNode *p)
	{
		removeNode(p);
		addToHead(p);
	}
	void addToHead(LinkedNode *p)
	{
		p->next = head->next;
		p->prev = head;
		head->next->prev = p;
		head->next = p;
		_size++;
	}
	LinkedNode *removeNode(LinkedNode *p)
	{
		p->prev->next = p->next;
		p->next->prev = p->prev;
		_size--;
		return p;
	}
};

//2021.3.23 -- 341. 扁平化嵌套列表迭代器
class NestedInteger
{
public:
	// Return true if this NestedInteger holds a single integer, rather than a nested list.
	bool isInteger() const;
	// Return the single integer that this NestedInteger holds, if it holds a single integer
	// The result is undefined if this NestedInteger holds a nested list
	int getInteger() const;
	// Return the nested list that this NestedInteger holds, if it holds a nested list
	// The result is undefined if this NestedInteger holds a single integer
	const vector<NestedInteger> &getList() const;
};
class NestedIterator
{
public:
	vector<NestedInteger>::const_iterator it;
	stack<vector<NestedInteger>::const_iterator> s;
	stack<vector<NestedInteger>::const_iterator> s_end;
	NestedIterator(vector<NestedInteger> &nestedList)
	{
		it = nestedList.begin();
		s.push(it);
		s_end.push(nestedList.end());
	}

	int next()
	{
		if (it->isInteger())
		{
			return it->getInteger();
		}
		else
		{
			it = it->getList().begin();
			s.push(it);
			return it->getList()[0].getInteger();
		}
	}

	bool hasNext()
	{
		if (s.size() == 0)
		{

			return false;
		}
		else
		{

			it = s.top();
			s.pop();
		}
	}
};

class Solution
{
public:
	//Code
	//2020.9.7--347. ǰ K ����ƵԪ��
	//	struct cmp_{
	//		bool cmp_(pair<int,int> m, pair<int,int> n){
	//			return m.second>n.second;
	//		}
	//	};
	//    vector<int> topKFrequent(vector<int>& nums, int k) {
	//		unordered_map<int,int> occur;
	//		for(int n:nums){
	//			occur[n]++;
	//		}
	//		priority_queue<pair<int,int>,vector<pair<int,int>>,cmp> q;
	//		for(auto [num,count]:occur){
	//			if(q.size()==k){
	//				if(q.top().second<count){
	//					q.pop();
	//					q.emplace(num,count);
	//				}
	//			}
	//			else{
	//				q.emplace(num,count);
	//			}
	//		}
	//		vector<int> ret;
	//		while(!q.empty()){
	//			ret.emplace_back(q.top().first);
	//			q.pop();
	//		}
	//		return ret;
	//    }

	//2020.9.8--77.���
	vector<vector<int>> combine(int n, int k)
	{
		if (n < k || n <= 0 || k <= 0)
			return vector<vector<int>>{};
		if (k == n)
		{
			vector<int> v;
			for (int i = 1; i <= n; i++)
			{
				v.push_back(i);
			}
			return vector<vector<int>>{v};
		}
		if (k == 1)
		{
			vector<vector<int>> ret;
			for (int i = 1; i <= n; i++)
			{
				vector<int> v{i};
				ret.push_back(v);
			}
			return ret;
		}

		vector<vector<int>> ret;
		vector<vector<int>> ret_n = mix(n, combine(n - 1, k - 1));
		vector<vector<int>> ret_rest = combine(n - 1, k);
		ret.insert(ret.end(), ret_n.begin(), ret_n.end());
		ret.insert(ret.end(), ret_rest.begin(), ret_rest.end());
		return ret;
	}
	vector<vector<int>> mix(int m, vector<vector<int>> vec)
	{
		vector<vector<int>> ret;
		for (int i = 0; i < vec.size(); i++)
		{
			vec[i].push_back(m);
		}
		return vec;
	}

	//2020.9.9--39.����ܺ�
	void dfs(vector<int> &candidates, int target, int id, vector<vector<int>> &ans, vector<int> &combine)
	{
		if (target == 0)
		{
			ans.push_back(combine);
			return;
		}
		if (candidates.size() <= id)
		{
			return;
		}
		dfs(candidates, target, id + 1, ans, combine);

		if (target - candidates[id] >= 0)
		{
			combine.push_back(candidates[id]);
			dfs(candidates, target - candidates[id], id, ans, combine);
			combine.pop_back();
		}
	}
	vector<vector<int>> combinationSum(vector<int> &candidates, int target)
	{
		//���������㷨
		vector<vector<int>> ans;
		vector<int> combine;
		dfs(candidates, target, 0, ans, combine);
		return ans;
	}

	//2020.9.10 -- 40.����ܺ�
	void dfs_40(vector<int> &candidates, int pos, int target, vector<vector<int>> &ans, vector<pair<int, int>> &freq, vector<int> &sequence)
	{
		if (target == 0)
		{
			ans.push_back(sequence);
			return;
		}
		if (target < freq[pos].first || pos == freq.size())
		{
			return;
		}
		dfs_40(candidates, pos + 1, target, ans, freq, sequence);

		int most = min(target / freq[pos].first, freq[pos].second);
		for (int i = 1; i <= most; i++)
		{
			sequence.push_back(freq[pos].first);
			dfs_40(candidates, pos + 1, target - i * freq[pos].first, ans, freq, sequence);
		}
		for (int i = 1; i <= most; ++i)
		{
			sequence.pop_back();
		}
	}
	vector<vector<int>> combinationSum2(vector<int> &candidates, int target)
	{
		vector<vector<int>> ans;
		vector<pair<int, int>> freq;
		vector<int> sequence;
		sort(candidates.begin(), candidates.end());
		for (int i = 0; i < candidates.size(); i++)
		{
			if (freq.empty() || candidates[i] != freq.back().first)
			{
				freq.emplace_back(candidates[i], 1);
			}
			else
			{
				freq.back().second++;
			}
		}
		dfs_40(candidates, 0, target, ans, freq, sequence);
		return ans;
	}

	//2020.9.11 -- 216.����ܺ�
	vector<vector<int>> dfs_216(int k, int n, int p)
	{
		vector<vector<int>> ans;
		if (n < 1 || n > 45 || k > 9 || k < 0)
			return ans;
		if (k == 1)
			ans.push_back(vector<int>{n});

		for (int i = p; i < min(9, n); i++)
		{
			vector<vector<int>> tmp = dfs_216(k - 1, n - i, i + 1);
			if (!tmp.empty())
			{
				//				ans.emplace_back(min(i, tmp));
			}
		}
	}
	vector<vector<int>> combinationSum3(int k, int n)
	{
		return dfs_216(k, n, 1);
	}

	//2021.3.20 -- 150. 逆波兰表达式求值
	int evalRPN(vector<string> &tokens)
	{
		stack<int> stk;
		int n = tokens.size();
		for (int i = 0; i < n; i++)
		{
			string &token = tokens[i];
			if (isNumber(token))
			{
				stk.push(atoi(token.c_str())); ///////////////////////////////////////////// string转int int num = atoi(str.c_str());
			}
			else
			{
				int num2 = stk.top();
				stk.pop();
				int num1 = stk.top();
				stk.pop();
				switch (token[0])
				{
				case '+':
					stk.push(num1 + num2);
					break;
				case '-':
					stk.push(num1 - num2);
					break;
				case '*':
					stk.push(num1 * num2);
					break;
				case '/':
					stk.push(num1 / num2);
					break;
				}
			}
		}
		return stk.top();
	}
	bool isNumber(string s)
	{
		int num = s[0] - '0';
		if (num >= 0 && num <= 9)
			return true;
		else
			return false;
	}

	//2021.3.21 -- 73. 矩阵置零
	void setZeroes(vector<vector<int>> &matrix)
	{
	}
	void propagate(vector<vector<int>> &matrix, vector<int> pos, vector<int> dir)
	{
		int size[2]{int(matrix.size()), int(matrix[0].size())};
		int row = pos[0];
		int col = pos[1];
		if (matrix[row][col] != 0)
		{
			matrix[row][col] = 0;
			if (pos[0] + dir[0] < size[0] && pos[0] + dir[0] >= 0 && pos[1] + dir[1] < size[1] && pos[1] + dir[1] >= 0)
				propagate(matrix, vector<int>{pos[0] + dir[0], pos[1] + dir[1]}, dir);
		}
		else
		{
			if (dir[0] != 0)
			{ //origin direction is horizontal,target direction is vertical
				vector<int> tar_dir{0, dir[1] + 1};
				if (pos[1] + tar_dir[1] < size[1] && matrix[pos[0]][pos[1] + tar_dir[1]] != 0)
				{
					propagate(matrix, vector<int>{pos[0] + dir[0], pos[1] + dir[1]}, dir);
				}
			}
			if (row - 1 >= 0 && matrix[row - 1][col] != 0)
			{
			}
		}
	}

	//2021.3.22 -- 191. 位1的个数
	int hammingWeight(uint32_t n)
	{
		//方法一
		int count = 0;
		for (int i = 0; i < 32; i++)
		{
			if ((n & (1 << i)) != 0)
			{
				count++;
			}
		}
		return count;
		//方法二
		// int count = 0;
		// while(n){
		// 	n=n&(n-1);
		// 	count++;
		// }
		// return count;
	}

	//2021.3.23 -- 青蛙跳台阶问题
	int numWays(int n)
	{
		long MAX = 1e9 + 7;
		if (n == 0 || n == 1)
		{
			return 1;
		}
		int pre = 1, cur = 2;
		for (int i = 3; i <= n; i++)
		{
			int tmp = (pre + cur) % MAX;
			pre = cur;
			cur = tmp;
		}
		return cur;
	}
	//2021.3.23 -- 单词拆分
	bool wordBreak(string s, vector<string> &wordDict)
	{
		int l = s.length();
		vector<int> dp(l + 1, 0);
		dp[l] = 1;
		for (int i = l - 1; i >= 0; i--)
		{
			for (vector<string>::iterator it = wordDict.begin(); it != wordDict.end(); ++it)
			{
				int offset = ifContain(s.substr(i, l - i), *it);
				if (offset != 0 && dp[i + offset] == 1)
				{
					dp[i] = 1;
					break;
				}
			}
		}
		return dp[0];
	}
	//返回两个字符串中字符不一致的位置索引
	int ifContain(string str, string s)
	{
		if (str.length() < s.length())
			return 0;
		int i = 0;
		for (; i < s.length(); ++i)
		{
			if (str[i] != s[i])
				return 0;
		}
		return s.length();
	}

	//2021.3.24 -- 机器人的运动范围
	bool ifLessK(int x, int y, int k)
	{
		int res = 0;
		for (; x; x /= 10)
		{
			res += x % 10;
		}
		for (; y; y /= 10)
		{
			res += y % 10;
		}
		return res <= k;
	}
	int get(int x)
	{
		int res = 0;
		for (; x; x /= 10)
		{
			res += x % 10;
		}
		return res;
	}
	int movingCount(int m, int n, int k)
	{
		if (!k)
			return 1;
		vector<vector<int>> flag(m, vector<int>(n, 0));
		int ans = 0;
		queue<vector<int>> pos_que;
		vector<int> dx{0, 1};
		vector<int> dy{1, 0};
		if (ifLessK(0, 0, k))
		{
			pos_que.push(vector<int>{0, 0});
			flag[0][0] = 1;
			ans++;
		}
		while (!pos_que.empty())
		{
			vector<int> pos = pos_que.front();
			pos_que.pop();
			for (int i = 0; i < 2; ++i)
			{
				int tx = pos[0] + dx[i];
				int ty = pos[1] + dy[i];
				if (tx < 0 || tx >= m || ty < 0 || ty >= n || flag[tx][ty] || !ifLessK(tx, ty, k))
					continue;
				pos_que.push(vector<int>{tx, ty});
				flag[tx][ty] = 1;
				ans++;
			}
		}
		return ans;
	}
	//2021.3.24 -- 分发饼干
	int findContentChildren(vector<int> &g, vector<int> &s)
	{
		sort(g.begin(), g.end());
		sort(s.begin(), s.end());
		int i = 0;
		int ans = 0;
		int num = s.size();
		for (auto c : g)
		{
			while (i < num && c > s[i])
			{
				i++;
			}
			if (i >= num)
			{
				break;
			}
			ans++;
			i++;
		}
		return ans;
	}
	//2021.3.24 -- 活字印刷
	void backtrack(string s, string rest, vector<string> &ans_vec)
	{
		if (rest.size() == 0)
			return;
		// int i=0;
		for (auto c : rest)
		{
			string s_new = s + c;
			if (find(ans_vec.begin(), ans_vec.end(), s_new) != ans_vec.end())
			{ //已有该字符串序列
				continue;
				// i++;
			}
			else
			{
				ans_vec.push_back(s_new);
				int i = find(rest.begin(), rest.end(), c) - rest.begin();
				backtrack(s_new, rest.erase(i, 1), ans_vec);
				rest.insert(rest.begin() + i, c);
			}
			// i++;
		}
	}
	int numTilePossibilities(string tiles)
	{
		vector<string> ans_vec;
		backtrack("", tiles, ans_vec);
		return ans_vec.size();
	}
	//在[0,N]范围内得到c个不重复的随机数
	int getLessN(vector<int> vec, int n)
	{
		int count = 0;
		for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
		{
			if (n >= *it)
			{
				n++;
				count++;
			}
			else
				break;
		}
		return count;
	}
	vector<int> getRandomN(int N, int c)
	{
		// int* ans = new int[c];
		vector<int> ans;
		srand((unsigned)time(NULL));
		int count = 0;
		while (count < c)
		{
			int randN = rand() % (N - count);
			int offset = getLessN(ans, randN);
			ans.insert(ans.begin() + offset, randN + offset);
			count++;
		}
		return ans;
	}
	// 264. 丑数 II
	int nthUglyNumber(int n)
	{
		set<long> heap;
		heap.emplace(1);
		auto it = heap.begin();
		for (; --n; ++it)
		{
			heap.emplace(*it * 2);
			heap.emplace(*it * 3);
			heap.emplace(*it * 5);
		}
		return *it;
	}
	bool isUglyNumber(int n, const unordered_set<long> &seen)
	{
		if (seen.count(n) != 0)
			return true;
		if (n == 1)
			return true;
		if (n % 2 == 0 && seen.count(n / 2) != 0)
			return true;
		if (n % 3 == 0 && seen.count(n / 3) != 0)
			return true;
		if (n % 5 == 0 && seen.count(n / 5) != 0)
			return true;
		return false;
	}
	int _nthUglyNumber(int n)
	{
		unordered_set<long> seen;
		int i = 1;
		for (int count = 0; count < n; i++)
		{
			if (isUglyNumber(i, seen))
			{
				seen.emplace(i);
				count++;
			}
		}
		return i - 1;
	}
	// 51.N皇后
	void putQueen(int row, int col, int n, vector<vector<int>> &occupied)
	{
		int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
		int dy[] = {1, 0, -1, 1, -1, 1, 0, -1};
		for (int i = 0; i < 8; i++)
		{
			int pos_x = col;
			int pos_y = row;
			for (int j = 0; j < n; j++)
			{
				pos_y += dy[i];
				pos_x += dx[i];
				if (pos_x >= 0 && pos_y >= 0 && pos_x < n && pos_y < n)
				{
					occupied[pos_y][pos_x] = 1;
				}
				else
				{
					break;
				}
			}
		}
	}
	void backtrackQueen(int row, int n, vector<vector<string>> &ans, vector<vector<int>> &occupied, vector<string> &place)
	{
		if (row == n - 1)
		{
			ans.push_back(place);
			return;
		}
		for (int i = 0; i < n; ++i)
		{
			if (occupied[row + 1][i] == 1)
			{
				continue;
			}
			place[row + 1][i] = 'Q';
			vector<vector<int>> tmp(occupied);
			putQueen(row + 1, i, n, occupied);
			backtrackQueen(row + 1, n, ans, occupied, place);
			place[row + 1][i] = '.';
			occupied = tmp;
		}
	}
	vector<vector<string>> solveNQueens(int n)
	{
		vector<vector<string>> ans;
		vector<vector<int>> occupied;
		vector<string> place;
		for (int i = 0; i < n; ++i)
		{
			occupied.push_back(vector<int>(n));
			for (int j = 0; j < n; ++j)
			{
				occupied[i].push_back(0);
			}
			place.push_back(string(n, '.'));
		}
		backtrackQueen(-1, n, ans, occupied, place);
		return ans;
	}
	// 190. 颠倒二进制位
	uint32_t reverseBits(uint32_t n)
	{
		uint32_t ans = 0;
		for (int i = 0; i < 32 && n; ++i)
		{
			ans |= (n & 1) << (32 - i);
			n >>= 1;
		}
		return ans;
	}

	//Code
};

class JZoffer
{
public:
	int findRepeatNumber(vector<int> &nums)
	{
		if (nums.size() < 2)
		{
			return -1;
		}
		set<int> s;
		for (auto n : nums)
		{
			auto ret = s.emplace(n);
			if (!ret.second)
			{
				return n;
			}
		}
		return -1;
	}
	int binarySearch(vector<int> vec, int target)
	{
		int low = 0, high = vec.size() - 1;
		while (low <= high)
		{
			int mid = (low + high) / 2;
			if (vec[mid] == target)
			{
				return mid;
			}
			else if (vec[mid] < target)
			{
				low = mid + 1;
			}
			else
			{ // vec[mid] > target
				high = mid - 1;
			}
		}
		return -1;
	}
	int binarySearchRightBoundary(vector<int> vec, int target)
	{
		int low = 0, high = vec.size() - 1;
		while (low < high)
		{
			int mid = (low + high) / 2;
			if (vec[mid] == target)
			{
				return mid;
			}
			else if (vec[mid] < target)
			{
				low = mid + 1;
			}
			else
			{ // vec[mid] > target
				high = mid;
			}
		}
		return low;
	}
	int binarySearchLeftBoundary(vector<int> vec, int target)
	{
		int low = 0, high = vec.size() - 1;
		while (low < high)
		{
			int mid = (low + high) / 2 + 1;
			if (vec[mid] == target)
			{
				return mid;
			}
			else if (vec[mid] < target)
			{
				low = mid;
			}
			else
			{ // vec[mid] > target
				high = mid - 1;
			}
		}
		return high;
	}
	// 278. 第一个错误的版本
	bool isBadVersion(int version);
	int firstBadVersion(int n)
	{
		if (n == 1)
		{
			return 1;
		}
		int low = 1, high = n;
		while (low < high)
		{
			int mid = low + (high - low) / 2;
			if (isBadVersion(mid))
			{
				high = mid;
			}
			else
			{
				low = mid + 1;
			}
		}
		return high;
	}
	// 153. 寻找旋转排序数组中的最小值
	int findMin_153(vector<int> &nums)
	{
		int low = 0, high = nums.size() - 1;
		int pivot = nums[0];
		while (low < high)
		{
			int mid = low + (high - low) / 2;
			if (nums[mid] >= pivot)
			{
				low = mid + 1;
			}
			else
			{
				high = mid;
			}
		}
		return nums[high] > pivot ? pivot : nums[high];
	}
	// 154. 寻找旋转排序数组中的最小值 II
	int binaryFindMin(vector<int> &nums, int low, int high)
	{
		if (low == high)
		{
			return nums[low];
		}
		int pivot = nums[low];
		while (low < high)
		{
			int mid = low + (high - low) / 2;
			if (nums[mid] > pivot)
			{
				low = mid + 1;
			}
			else if (nums[mid] == pivot)
			{
				return min(binaryFindMin(nums, low, mid), binaryFindMin(nums, mid + 1, high));
			}
			else
			{
				high = mid;
			}
		}
		return nums[high] > pivot ? pivot : nums[high];
	}
	int findMin(vector<int> &nums)
	{
		int low = 0, high = nums.size() - 1;
		return binaryFindMin(nums, low, high);
	}
	// 34. 在排序数组中查找元素的第一个和最后一个位置
	vector<int> searchRange(vector<int> &nums, int target)
	{
		if (nums.size() == 0)
		{
			return vector<int>{-1, -1};
		}
		int low1 = 0, low2 = 0, high1 = nums.size() - 1, high2 = nums.size() - 1;
		while (low1 < high1 || low2 < high2)
		{
			int mid1 = low1 + (high1 - low1) / 2;
			int mid2 = low2 + (high2 - low2 + 1) / 2;
			if (low1 < high1)
			{
				if (nums[mid1] < target)
				{
					low1 = mid1 + 1;
				}
				else
				{
					high1 = mid1;
				}
			}
			if (low2 < high2)
			{
				if (nums[mid2] <= target)
				{
					low2 = mid2;
				}
				else
				{
					high2 = mid2 - 1;
				}
			}
		}
		if (nums[high1] != target)
		{
			return vector<int>{-1, -1};
		}
		return vector<int>{high1, low2};
	}
	// 162. 寻找峰值
	int findPeakElement(vector<int> &nums)
	{
		if (nums.size() == 0)
		{
			return -1;
		}
		int maxN = nums[0];
		int ind = 0;
		bool flag = false;
		for (int i = 0; i < nums.size() - 1; ++i)
		{
			if (nums[i + 1] > maxN)
			{
				maxN = nums[i + 1];
				ind = i + 1;
			}
			if (nums[i] < nums[i + 1])
			{
				flag = true;
			}
			else
			{
				if (flag == true)
				{
					return i;
				}
				flag = false;
			}
		}
		return ind;
	}
	// 3. 无重复字符的最长子串
	int lengthOfLongestSubstring(string s)
	{
		if (s.size() == 0)
		{
			return 0;
		}
		map<char, int> m;
		int i = 0, j = 0, max_len = 0;
		for (; j < s.size(); ++j)
		{
			if (m.find(s[j]) != m.end())
			{
				i = max(i, m[s[j]] + 1);
			}
			m[s[j]] = j;
			if (j - i + 1 > max_len)
			{
				max_len = j - i + 1;
			}
		}
		return max_len;
	}
	// 393. UTF-8 编码验证
	int getBytesNum(int byte)
	{
		int i = 0, num = 0;
		for (; i < 5; i++)
		{
			if ((byte >> (7 - i) & 1) == 0)
			{
				break;
			}
		}
		if (i == 0)
			return 1;
		if (i > 4 || i == 1)
			return -1;
		return i;
	}
	bool ifVaildByte(int byte)
	{
		return (byte >> (7 - 0) & 1) == 1 && (byte >> (7 - 1) & 1) == 0;
	}
	bool validUtf8(vector<int> &data)
	{
		//data = [197, 130, 1], 表示 8 位的序列: 11000101 10000010 00000001.
		int i = 0, num = 0;
		while (i < data.size())
		{
			num = getBytesNum(data[i]);
			if (num == -1)
				return false;
			cout << num;
			num--;
			i++;

			while (num > 0)
			{
				if (i == data.size() || !ifVaildByte(data[i]))
				{
					return false;
				}
				num--;
				i++;
			}
		}
		return true;
	}
	// 306. 累加数
	bool backtrack_306(int nums[3], int ind, string str_num, int str_ind)
	{
		if (ind == 3)
		{
			if (nums[0] + nums[1] == nums[2])
			{
				cout << nums[0] << nums[1] << nums[2] << endl;
				if (str_ind == str_num.size())
				{
					return true;
				}
				nums[0] = nums[1];
				nums[1] = nums[2];
				nums[2] = -1;
				ind = 1;
				return backtrack_306(nums, ind + 1, str_num, str_ind);
			}
			else
				return false;
		}
		for (int i = str_ind; i < str_num.size(); ++i)
		{
			if (nums[ind] < 0)
			{
				nums[ind] = str_num[i] - '0';
			}
			else
			{
				if (nums[ind] == 0)
				{
					break;
				}
				nums[ind] = nums[ind] * 10 + str_num[i] - '0';
			}
			if (!backtrack_306(nums, ind + 1, str_num, i + 1))
			{
				if (ind != 2)
				{
					nums[ind + 1] = -1;
					continue;
				}
				else if (nums[0] + nums[1] < nums[2])
				{
					break;
				}
			}
			else
				return true;
		}
		return false;
	}
	bool isAdditiveNumber(string num)
	{
		int nums[3] = {-1, -1, -1};
		return backtrack_306(nums, 0, num, 0);
	}

	// end
};

// class A{
// 	int a;
// public:
// 	A(){a=0;}
// 	void operator++(){//一元运算符
// 		a+=1;
// 	}
// 	void operator++(int){//二元运算符
// 		a+=2;
// 	}
// 	friend void print(A c);
// };
// void print(A c){
// 	cout << c.a << endl;
// }
inline int func(int x) { return x * x; }
class A
{
public:
	A()
	{
		cout << "构造函数A" << endl;
	}
	void func()
	{
		cout << "a f";
	}
	virtual void vfunc()
	{
		cout << "A V F";
	};
	virtual ~A();
};
class B : public A
{
public:
	B()
	{
		cout << "构造函数B" << endl;
	}
	void func()
	{
		cout << "b f";
	}
	void vfunc()
	{
		cout << "B V F";
	}
	~B();
	// ~B(){cout<<"析构函数B"<<endl;};
};
A::~A()
{
	cout << "纯虚析构函数A" << endl;
}
B::~B()
{
	cout << "析构函数B" << endl;
}

class newCoder
{
public:
};

const char *clone_str(const char *in_str)
{
	char *out_str = new char[100];
	int i = 0;
	while (in_str[i] != '\0')
	{
		out_str[i] = in_str[i];
		++i;
	}
	out_str[i] = '\0';
	return out_str;
}

int main(int argc, char *argv[])
{
	newCoder nc;
	JZoffer jz;
	string s = "12122436";
	cout << jz.isAdditiveNumber(s);
	return 0;
}