# C++

## const_iterator 和 const vector<>::iterator

vector<int>::iterator iter 相当于 int *p

vector<int>::const_iterator iter 相当于 int const *p，就是地址下的实际值是不能改变的常量

```C++
vector<int> i(10,1);       
vector<int>::iterator iter = i.begin();
*iter = 10;//可以
vector<int>::const_iterator iter = i.begin();
*iter = 10;//不能
```

```C
int a[10] = {1};
int *p = a;
*p = 2;//可以

const int *p = a;
*p = 2;//不可以
```

const vector<int>::iterator iter 相当于C中的int *const p 就是指针值不能变，地址下的值能变

```C++
vector<int>::iterator iter = i.begin;
iter++;//可以
 
vector<int> i(10,1);
const vector<int>::iterator iter = i.begin;
iter++;//不可以
```

```C
int a[10] = {1};
int *p = a;
p++;//可以
 
int *const p;
p++;//不可以
```

## 虚函数

定义一个函数为虚函数，不代表函数为不被实现的函数。
定义他为虚函数是为了允许用基类的指针来调用子类的这个函数。
定义一个函数为**纯虚函数**，才代表函数没有被实现。

纯虚函数是在基类中声明的虚函数，它在基类中没有定义，但要求任何派生类都要定义自己的实现方法。在基类中实现纯虚函数的方法是在函数原型后加“=0”

```c++
virtual void funtion1()=0
```

引入原因：

1、为了方便使用**多态**特性，我们常常需要在基类中定义虚拟函数。
2、在很多情况下，基类本身生成对象是不合情理的。例如，动物作为一个基类可以派生出老虎、孔雀等子类，但动物本身生成对象明显不合常理。

`含有纯虚拟函数的类称为抽象类，它不能生成对象`

纯虚函数的作用：

1、为了安全，因为避免任何需要明确但是因为不小心而导致的未知的结果，提醒子类去做应做的实现。
2、为了效率，不是程序执行的效率，而是为了编码的效率。

## 多态

多态是在不同继承关系的类对象，去调同一函数，产生了不同的行为。

就是说，有一对继承关系的两个类，这两个类里面都有一个函数且名字、参数、返回值均相同，然后我们通过调用函数来实现不同类对象完成不同的事件。

但是构成多态还有两个条件：

调用函数的对象必须是指针或者引用。
被调用的函数必须是虚函数，且完成了虚函数的重写。

[C++ 之 多态](https://blog.csdn.net/qq_39412582/article/details/81628254?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control)

![这里写图片描述](MyNotes.assets/20180813114655864)

**静态多态**是编译器在**编译期间**完成的，编译器会根据实参类型来选择调用合适的函数，如果有合适的函数可以调用就调，没有的话就会发出警告或者报错。

**动态多态：** 显然这和静态多态是一组反义词，它是在**程序运行时**根据基类的引用（指针）指向的对象来确定自己具体该调用哪一个类的虚函数。

>  哪些函数不能定义为虚函数？
> 经检验下面的几个函数都不能定义为虚函数：
> 1）友元函数，它不是类的成员函数
> 2）全局函数
> 3）静态成员函数，它没有this指针
> 3）构造函数，拷贝构造函数，以及赋值运算符重载（可以但是一般不建议作为虚函数）
>
> 多态缺陷？
> 1）降低了程序运行效率（多态需要去找虚表的地址）
> 2）空间浪费

## static、const和volatile关键字的作用
### static:

1. 修饰普通变量，修改变量的存储区域和生命周期，使变量存储在静态区，在main函数运行前就分配了空间，如果有初始值就用初始值初始化它，如果没有初始值系统用默认值初始化它。在每次调用时，其值为上一次调用后改变的值，调用结束后不释放空间。此变量只在声明变量的文件内可见。
2. 修饰普通函数，表明函数的作用范围，仅在定义该函数的文件内才能使用。在多人开发项目时，为了防止与他人命令函数重名，可以将函数定义为static。
3. 修饰成员变量，修饰成员变量使所有的对象只保存一个该变量，而且不需要生成对象就可以访问该成员。
4. 修饰成员函数，修饰成员函数使得不需要生成对象就可以访问该函数，但是在static函数内不能访问非静态成员。

> 静态成员变量可以在一个类的所有实例间共享数据。

### const:

1. 修饰变量，说明该变量不可以被改变；
2. 修饰指针，分为指向常量的指针和指针常量；
3. 常量引用，经常用于形参类型，即避免了拷贝，又避免了函数对值的修改；
4. 修饰成员函数，说明该成员函数内不能修改成员变量。

### volatile：

**volatile提醒编译器它后面所定义的变量随时都有可能改变**，因此编译后的程序每次需要存储或读取这个变量的时候，都会直接从变量地址中读取数据。如果没有volatile关键字，则**编译器可能优化读取和存储，暂时使用寄存器中的值**，如果这个变量由别的程序更新了的话，将出现不一致的现象。

![img](MyNotes.assets/20190619170056964.png)

这段程序等待内存变量flag的值变为1(怀疑此处是0,有点疑问,)之后才运行do2()。变量flag的值由别的程序更改，**这个程序可能是某个硬件中断服务程序。**例如：如果某个按钮按下的话，就会对DSP产生中断，在按键中断程序中修改flag为1，这样上面的程序就能够得以继续运行。但是，**编译器并不知道flag的值会被别的程序修改，因此在它进行优化的时候，可能会把flag的值先读入某个寄存器，然后等待那个寄存器变为1。**如果不幸进行了这样的优化，那么while循环就变成了死循环，因为寄存器的内容不可能被中断服务程序修改。为了让程序每次都读取真正flag变量的值，就需要定义为如下形式：`volatile short flag;`

volatile的本意是“易变的”,由于访问寄存器的速度要快过RAM，所以编译器一般都会作减少存取外部RAM的优化。



## 野指针

野指针就是指向一个已删除的对象或者未申请访问受限内存区域的指针。



## 智能指针

| std        | boost      | 功能说明                                                     |
| ---------- | ---------- | ------------------------------------------------------------ |
| auto_ptr   | auto_ptr   | 动态分配对象以及当对象不再需要时自动执行清理。               |
| unique_ptr | scoped_ptr | 独占指针对象，并保证指针所指对象生命周期与其一致。           |
| shared_ptr | shared_ptr | 可共享指针对象，可以赋值给shared_ptr或weak_ptr。 指针所指对象在所有的相关联的shared_ptr生命周期结束时结束，是强引用。 |
| weak_ptr   | weak_ptr   | 它不能决定所指对象的生命周期，引用所指对象时，需要lock()成shared_ptr才能使用。**主要为了解决shared_ptr的循环引用问题** |

```C++
void func(){
    auto_ptr<string> ps (new string("nihao"));
    auto_ptr<string> vocation;
    vocation = ps;
}
//为了避免一个对象被删除多次，提出了unique_ptr和shared_ptr这两种智能指针。
```

## 右值引用、移动语义

> https://www.jianshu.com/p/d19fc8447eaa

`c++11`中的右值引用使用的符号是`&&`，如

```cpp
int&& a = 1; //实质上就是将不具名(匿名)变量取了个别名
int b = 1;
int && c = b; //编译错误！ 不能将一个左值复制给一个右值引用
class A {
  public:
    int a;
};
A getTemp()
{
    return A();
}
A && a = getTemp();   //getTemp()的返回值是右值（临时变量）
```

`getTemp()`返回的右值本来在表达式语句结束后，其生命也就该终结了（因为是临时变量），而通过右值引用，该右值又重获新生，其生命期将与右值引用类型变量`a`的生命期一样，只要`a`还活着，该右值临时变量将会一直存活下去。实际上就是给那个临时变量取了个名字。

```cpp
int x = 10;
int y = 23;
int && r1 = 13;
int && r2 = x+y;
double && r3 = std::sqrt(2.0);
```

**右值包括字面常量、诸如x+y等表达式以及返回值的函数（条件是该函数返回的不是引用）**

>  常量左值， 使用 `const T&`, 既可以绑定**左值**又可以绑定**右值**

```cpp
#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

class MyString
{
public:
    static size_t CCtor; //统计调用拷贝构造函数的次数
    static size_t MCtor; //统计调用移动构造函数的次数
    static size_t CAsgn; //统计调用拷贝赋值函数的次数
    static size_t MAsgn; //统计调用移动赋值函数的次数

public:
    // 构造函数
   MyString(const char* cstr=0){
       if (cstr) {
          m_data = new char[strlen(cstr)+1];
          strcpy(m_data, cstr);
       }
       else {
          m_data = new char[1];
          *m_data = '\0';
       }
   }

   // 拷贝构造函数
   MyString(const MyString& str) {
       CCtor ++;
       m_data = new char[ strlen(str.m_data) + 1 ];
       strcpy(m_data, str.m_data);
   }
   // 移动构造函数
   MyString(MyString&& str) noexcept
       :m_data(str.m_data) {
       MCtor ++;
       str.m_data = nullptr; //不再指向之前的资源了
   }

   // 拷贝赋值函数 =号重载
   MyString& operator=(const MyString& str){
       CAsgn ++;
       if (this == &str) // 避免自我赋值!!
          return *this;

       delete[] m_data;
       m_data = new char[ strlen(str.m_data) + 1 ];
       strcpy(m_data, str.m_data);
       return *this;
   }

   // 移动赋值函数 =号重载
   MyString& operator=(MyString&& str) noexcept{
       MAsgn ++;
       if (this == &str) // 避免自我赋值!!
          return *this;

       delete[] m_data;
       m_data = str.m_data;
       str.m_data = nullptr; //不再指向之前的资源了
       return *this;
   }

   ~MyString() {
       delete[] m_data;
   }

   char* get_c_str() const { return m_data; }
private:
   char* m_data;
};
size_t MyString::CCtor = 0;
size_t MyString::MCtor = 0;
size_t MyString::CAsgn = 0;
size_t MyString::MAsgn = 0;
int main()
{
    vector<MyString> vecStr;
    vecStr.reserve(1000); //先分配好1000个空间
    for(int i=0;i<1000;i++){
        vecStr.push_back(MyString("hello"));
    }
    cout << "CCtor = " << MyString::CCtor << endl;
    cout << "MCtor = " << MyString::MCtor << endl;
    cout << "CAsgn = " << MyString::CAsgn << endl;
    cout << "MAsgn = " << MyString::MAsgn << endl;
}

/* 结果
CCtor = 0
MCtor = 1000
CAsgn = 0
MAsgn = 0
*/
```

可以看到，移动构造函数与拷贝构造函数的区别是，拷贝构造的参数是`const MyString& str`，是*常量左值引用*，而移动构造的参数是`MyString&& str`，是*右值引用*，而`MyString("hello")`是个临时对象，是个右值，优先进入移动构造函数而不是拷贝构造函数。而**移动构造函数与拷贝构造不同，它并不是重新分配一块新的空间，将要拷贝的对象复制过来，而是"偷"了过来，将自己的指针指向别人的资源，然后将别人的指针修改为`nullptr`，这一步很重要，如果不将别人的指针修改为空，那么临时对象析构的时候就会释放掉这个资源**，"偷"也白偷了。

### noexcept

**noexcept形如其名地，表示其修饰的函数不会抛出异常。**因为异常机制会带来一些额外开销，比如函数抛出异常，会导致函数栈被依次地展开（unwind），并依帧调用在本帧中已构造的自动变量的析构函数等。
从语法上讲，noexcept修饰符有两种形式，一种就是简单地在函数声明后加上noexcept关键字。比如：

```cpp
void excpt_func() noexcept
```


另外一种则可以接受一个常量表达式作为参数，如下所示：

```cpp
void excpt_func() noexcept(常量表达式);
```


常量表达式的结果会被转换成一个bool类型的值。该值为true，表示函数不会抛出异常，反之，则有可能抛出异常。这里，不带常量表达式的noexcept相当于声明了noexcept(true)，即不会抛出异常。

在通常情况下，在C++11中使用noexcept可以有效地阻止异常的传播与扩散。



## 句柄

>  **句柄和指针的区别在于句柄只能调用系统提供的服务。**

​		首先说**指针吧。通俗一点就是地址，他是内存的编号，通过它我们可以直接对内存进行操作，只要地址不变，我们每次操作的物理位置是绝对不变，记住这句话，这是句柄和指针的重大区别所在。**
　　再说说**句柄吧，一般是指向系统的资源的位置，可以说也是地址**。但是这些资源的位置真的不变，我们都知道window支持虚拟内存的技术，同一时间内可能有些资源被换出内存，一些被换回来，这就是说同一资源在系统的不同时刻，他在内存的物理位置是不确定的，那么window是如何解决这个问题呢，就是通过句柄来处理资源的物理位置不断变化的这个问题的。window会在物理位置固定的区域存储一张对应表，表中记录了所有的资源实时地址，**句柄其实没有直接指向资源的物理地址，而是指向了这个对应表中的一项，这样无论资源怎样的换进换出，通过句柄都可以找到他的实时位置。**
　　总的来说，**通过句柄可以屏蔽系统内部的细节，让程序设计可以不必考虑操作系统实现的细节**。



## 纯虚函数、虚继承、纯虚析构函数

```C++
virtual void func()=0;//纯虚函数，我们把包含纯虚函数的类称之为抽象类。纯虚函数必须在子类中被实现。
```

### [虚继承](http://c.biancheng.net/view/2280.html)

```C++
//间接基类A
class A{
protected:
    int m_a;
};
//直接基类B
class B: public A{
protected:
    int m_b;
};
//直接基类C
class C: public A{
protected:
    int m_c;
};
//派生类D
class D: public B, public C{
public:
    void seta(int a){ m_a = a; }  //命名冲突
    void setb(int b){ m_b = b; }  //正确
    void setc(int c){ m_c = c; }  //正确
    void setd(int d){ m_d = d; }  //正确
private:
    int m_d;
};
int main(){
    D d;
    return 0;
}
```

为了解决多继承时的命名冲突和冗余数据问题，[C++](http://c.biancheng.net/cplus/) 提出了虚继承，使得在派生类中只保留一份间接基类的成员。

虚继承的目的是让某个类做出声明，承诺愿意共享它的基类。其中，这个被共享的基类就称为虚基类（Virtual Base Class），本例中的 A 就是一个虚基类。在这种机制下，不论虚基类在继承体系中出现了多少次，在派生类中都只包含一份虚基类的成员。

![菱形继承和虚继承](MyNotes.assets/1-2006291J3551E.png)

#### [虚继承是否能完全替代普通继承？](https://blog.csdn.net/dqjyong/article/details/8029527)

不行，虚继承已完全破坏了继承体系，不能按照平常的继承体系来进行类型转换。不管怎么样，虚继承在类型转换是一定要十分注意。不要轻易使用虚继承，更不要在虚继承的基础上进行类型转换，切记切记！

### [虚析构函数](https://blog.csdn.net/wufeifan_learner/article/details/88804072)

```C++
#include <iostream>
using namespace std;
class A{
	public:
		A(){
			cout<<"构造函数A"<<endl;
		}
		~A(){
			cout<<"析构函数A"<<endl;
		}
};
class B: public A{
	public:
		B(){
			cout<<"构造函数B"<<endl;
		}
		~B(){
			cout<<"析构函数B"<<endl;
		}
};
int main(){
	A *a = new B();
	delete a;
}
/*
结果：
构造函数A
构造函数B
析构函数A
--------
可见，如此声明并不会调用派生类B的析构函数，那么，在delete的时候会造成内存泄露。
*/
```

**纯虚析构函数**其实和**虚析构函数**无差别，就是声明之后，**必须在类外实现**的析构函数而已。只是这个析构函数不能在子类中实现，必须在类外实现。

**虚析构函数**就是为了保证如上例情况时释放时也执行子类析构函数，避免内存泄漏。



## 为什么(待继承的类的)析构函数必须是虚函数？为什么C++默认的析构函数不是虚函数？

将可能会被继承的父类的析构函数设置为虚函数，可以保证当我们new一个子类，然后使用基类指针指向该子类对象，释放基类指针时可以释放掉子类的空间，防止内存泄漏。

C++默认的析构函数不是虚函数是因为虚函数需要额外的虚函数表和虚表指针，占用额外的内存。而对于不会被继承的类来说，其析构函数如果是虚函数，就会浪费内存。因此C++默认的析构函数不是虚函数，而是**只有当需要当作父类时，设置为虚函数**。

>  C++类的六个默认成员函数：
> **构造函数**：一个特殊的成员函数，名字与类名相同，创建类类型对象的时候，由编译器自动调用，在对象的生命周期内只且调用一次，以保证每个数据成员都有一个合适的初始值。
> **拷贝构造函数**：只有单个形参，而且该形参是对本类类型对象的引用（常用const修饰），这样的构造函数称为拷贝构造函数。拷贝构造函数是特殊的构造函数，创建对象时使用已存在的同类对象来进行初始化，由编译器自动调用。
> **析构函数**：与构造函数功能相反，在对象被销毁时，由编译器自动调用，完成类的一些资源清理和收尾工作。
> **赋值运算符重载**：对于类类型的对象我们需要对‘=’重载，以完成类类型对象之间的赋值。
> **取址操作符重载**：函数返回值为该类型的指针，无参数。
> **const修饰的取址运算符重载**。



## 拷贝构造函数（copy constructor）

默认构造函数（即无参构造函数）不一定存在，但是复制构造函数总是会存在。

**拷贝构造函数**是一种特殊的构造函数，它在创建对象时，是使用同一类中之前创建的对象来初始化新创建的对象。拷贝构造函数通常用于：

- 通过使用另一个同类型的对象来初始化新创建的对象。
- 复制对象把它作为参数传递给函数。
- 复制对象，并从函数返回这个对象。

### 拷贝构造函数与赋值运算符(=)有何不同？

​	赋值运算符*(=)*作用于一个**已存在的对象**；而拷贝构造函数会**创建一个新的对象**。

## RTTI、typeid、type_info

RTTI（Runtime Type Identification）：运行阶段类型识别

typied运算符返回一个指出对象的类型的值

type_info结构存储了有关特定类型的信息

dynamic_cast 运算符将使用一个指向基类的指针来生成一个指向派生类的指针；否则，该运算符返回0——空指针

```C++
//现在我们有这样两个类
class Base
{
public:
    virtual void show(){}
};

class Derive:public Base
{
public:
    virtual void show(){}
};
```

```C++
Base* bptr = new Derive();
//Derive* dptr = bptr;   //error
Derive* dptr = dynamic_cast<Derive*>(bptr);  //没问题
//dptr != NULL
```

```C++
Base* bptr = new Base();
//Derive* dptr = bptr;  //error编译不过

//编译可以过去，但是我们来看看dptr的值
Derive* dptr = dynamic_cast<Derive*>(bptr);
cout<<"dptr = "<<dptr<<endl; //dptr == 0000000;
```



## 泛型编程

所以泛型，实质上就是不使用具体数据类型（例如 int、double、float 等），而是使用一种通用类型来进行程序设计的方法，该方法可以大规模的减少程序代码的编写量，让程序员可以集中精力用于业务逻辑的实现。



## 内联函数

为了**解决一些频繁调用的小函数大量消耗栈空间（栈内存）的问题**，特别的引入了inline修饰符，表示为内联函数，栈空间就是指放置程序的局部数据（也就是函数内数据）的内存空间。在系统下，栈空间是有限的，假如频繁大量的使用就会造成因栈空间不足而导致程序出错的问题，如，函数的死循环递归调用的最终结果就是导致栈内存空间枯竭。

```C++
#include <stdio.h>
#include <string.h>
// 函数定义为inline即:内联函数
inline char* inline_test(int num) 
{
    return (num % 2 > 0) ? "奇" : "偶";
} 
int main()
{
   int i = 0;
   for (i = 1; i < 10; i++) 
   {
       printf("inline_test:   i:%d   奇偶性:%s\n", i, inline_test(i));   
   }   
   return 0;
}
```

```cpp
//如下风格的函数Foo 不能成为内联函数： 
inline void Foo(int x, int y); // inline 仅与函数声明放在一起
void Foo(int x, int y){}
//而如下风格的函数Foo 则成为内联函数：
void Foo(int x, int y);
inline void Foo(int x, int y) {} // inline 与函数定义体放在一起
```

 以下情况不宜使用内联： 
(1) 如果函数体内的**代码比较长**，使用内联将导致内存消耗代价较高。 
(2) 如果函数体内**出现循环**，那么执行函数体内代码的时间要比函数调用的开销大。
(3) 类的构造函数和析构函数容易让人误解成使用内联更有效。

 

## Public、Protected、Private

### 成员属性：（访问权限）

1. **public：**类内外、派生类对象**都可以**访问public成员。
2. **protected：**protected成员可以被**派生类对象**访问，不能被类外访问。
3. **private**：private成员只能被本类成员（类内）和**友元**访问，不能被派生类访问；

```text
C++中struct和class意义相同，唯一不同就是struct默认的访问控制是public，class中默认的访问控制是private（同继承）。
```

### 继承：（基类成员访问属性在派生类中发生变化）

**1.public继承：**基类public成员，protected成员，private成员的访问属性在派生类中分别变成：public, protected, （不可访问）

**2.protected继承：**基类public成员，protected成员，private成员的访问属性在派生类中分别变成：protected, protected, （不可访问）

**3.private继承：**基类public成员，protected成员，private成员的访问属性在派生类中分别变成：private, private, （不可访问）

```text
如果不指定public，C++默认的是私有继承（private）。
```



## 运算符重载

```C++
class A{
	int a;
public:
	A(){a=0;}
	void operator++(){//一元运算符，++a
		a+=1;
	}
	void operator++(int){//二元运算符，a++
		a+=2;
		return a;
	}
	friend void print(A c);
};
void print(A c){
	cout << c.a << endl;
}
int main(int argc, char *argv[])
{
	A a;
	print(a);//0
	a++;//二元运算符
	print(a);//2
	++a;//一元运算符
	print(a);//3
}
```

###  不能被重载的运算符只有五个，分别是

1. "."（成员访问运算符）

2. “ * ”（成员指针访问运算符）

3. :: （域运算符）

4. sizeof （长度运算符）

5. ?: （条件运算符）

前两个运算符不能重载是为了保证访问成员的功能不被改变 ，域运算符和sizeof运算符的运算对象是类型而不是变量或者一般表达式，不具备重载的特征。

## 一个程序从开始运行到结束的完整过程（四个过程）

预处理：条件编译，头文件包含，宏替换的处理，生成.i文件。

编译：将预处理后的文件转换成汇编语言，生成.s文件

汇编：汇编变为目标代码(机器代码)生成.o的文件

链接：连接目标代码,生成可执行程序

## C++序列化

### 1.什么是序列化

在编写应用程序的时候往往须要将程序的某些数据存储在内存中，然后将其写入某个文件或是将它传输到网络中的还有一台计算机上以实现通讯。这个**将程序数据转化成能被存储并传输的格式的过程被称为“序列化”（Serialization）**，而它的逆过程则可被称为“反序列化”（Deserialization）。

简单来说，**序列化就是将对象实例的状态转换为可保持或传输的格式的过程。**与序列化相对的是反序列化，它依据流重构对象。这两个过程结合起来，能够轻松地存储和数据传输。比如，能够序列化一个对象，然后使用 HTTP 通过 Internet 在client和server之间传输该对象。

**总结**

序列化：将对象变成字节流的形式传出去。

反序列化：从字节流恢复成原来的对象。

### 2. 为什么要序列化？优点在哪里？

简单来说，对象序列化通经常使用于两个目的： 

（1） 将对象存储于硬盘上 ，便于以后反序列化使用

（2）在网络上传送对象的字节序列

对象序列化的优点在哪里？**网络传输方面的便捷性、灵活性**就不说了，这里举个我们常常可能发生的需求：你有一个数据结构，里面存储的数据是经过非常多其他数据通过非常复杂的算法生成的，因为数据量非常大，算法又复杂，因此生成该数据结构所用数据的时间可能要非常久（或许几个小时，甚至几天），生成该数据结构后又要用作其他的计算，那么你在调试阶段，每次执行个程序，就光生成数据结构就要花上这么长的时间，无疑代价是非常大的。假设你确定生成数据结构的算法不会变或不常变，那么就**能够通过序列化技术生成数据结构数据存储到磁盘上，下次又一次执行程序时仅仅须要从磁盘上读取该对象数据就可以，所花费时间也就读一个文件的时间**，可想而知是多么的快，节省了我们的开发时间。

### 3.C++对象序列化的四种方法

- Google Protocol Buffers (GPB)是Google内部使用的数据编码方式，旨在用来取代XML进行数据交换。可用于数据序列化与反序列化。主要特性有： 高效、语言中立(Cpp, Java, Python)、可扩展 

- Boost.Serialization能够创建或重建程序中的等效结构，并保存为二进制数据、文本数据、XML或者实用户自己定义的其它文件，既支持二维数组（指针），也支持STL容器。**如orb-slam中的map_file**

  ```C++
  void Save()
  {
  	/*打开Test.bin 此种方法是利用构造函数打开*/
  	std::ofstream file("Test.bin");
  
  	/*定义oa为二进制存储类型*/
  	boost::archive::text_oarchive oa(file);
  	//boost::archive::binary_oarchive oa(file);
  
  	int nNum = 0;
  	std::cout << "输入int型" << std::endl;
  	std::cin >> nNum;
  
  	float fFloat = 0.0f;
  	std::cout << "输入float型" << std::endl;
  	std::cin >> fFloat;
  
  	oa << nNum << fFloat;
  
  	file.close();
  }
  ```

- MFC Serialization：Windows平台下可使用MFC中的序列化方法。MFC 对 CObject 类中的序列化提供内置支持。因此，全部从 CObject 派生的类都可利用 CObject 的序列化协议。
-  .Net Framework：.NET的执行时环境用来支持用户定义类型的流化的机制。它在此过程中，先将对象的公共字段和私有字段以及类的名称（包含类所在的程序集）转换为字节流，然后再把字节流写入数据流。在随后对对象进行反序列化时，将创建出与原对象全然同样的副本。

> - Google Protocol Buffers效率较高，可是数据对象必须预先定义，并使用protoc编译，适合要求效率，同意自己定义类型的内部场合使用。
> - Boost.Serialization 使用灵活简单，并且支持标准C++容器。
> - 相比而言，MFC的效率较低，可是结合MSVS平台使用最为方便。

## C++ 内存分配

**1.内存分配方式**
内存分配方式有三种：

- [1]从静态存储区域分配。内存在程序编译的时候就已经分配好，这块内存在程序的整个运行期间都存在。例如全局变量，static变量。
- [2]在栈上创建。 在执行函数时，函数内局部变量的存储单元都可以在栈上创建，函数执行结束时这些存储单元自动被释放。栈内存分配运算内置于处理器的指令集中，效率很高，但是分配的内存容量有限。
- [3]从堆上分配，亦称**动态内存分配**。程序在运行的时候用malloc或new申请任意多少的内存，程序员自己负责在何时用free或delete释放内存。动态内存的生存期由程序员决定，使用非常灵活，但如果在堆上分配了空间，就有责任回收它，否则运行的程序会出现内 存泄漏，频繁地分配和释放不同大小的堆空间将会产生堆内碎块。 

**2.程序的内存空间**
一个由C/C++编译的程序占用的内存分为以下几个部分：
1、栈区（stack）—　 由编译器自动分配释放 ，存放为运行函数而分配的**局部变量、函数参数、返回数据、返回地址**等。其操作方式类似于数据结构中的栈。
2、堆区（heap） —　 一般由程序员分配释放， 若程序员不释放，程序结束时可能由OS回收 。分配方式类似于链表。
3、全局区（静态区）（static） —存放全局变量、静态数据、常量。程序结束后由系统释放。
4、文字常量区 —常量字符串就是放在这里的。 程序结束后由系统释放。
5、程序代码区—存放函数体（类成员函数和全局函数）的二进制代码。

3.堆区与栈区的比较

- 申请方式
  stack: 由系统自动分配。 例如，声明在函数中一个局部变量 int b; 系统自动在栈中为b开辟空间。
  heap: 需要程序员自己申请，并指明大小，在C中malloc函数，C++中是new运算符。
  如p1 = (char *)malloc(10); p1 = new char[10];
  如p2 = (char *)malloc(10); p2 = new char[20];
  但是注意p1、p2本身是在栈中的。
- 申请后系统的响应
  栈：只要栈的剩余空间大于所申请空间，系统将为程序提供内存，否则 将报异常提示栈溢出。
  堆：首先应该知道操作系统有一个记录空闲内存地址的链表，当系统收到程序的申请时，会遍历该链表，寻找第一个空间大于所 申请空间的堆结点，然后将该结点从空闲结点链表中删除，并将该结点的空间分配给程序。
  **对于大多数系统，会在这块内存空间中的首地址处记录本次分配的大小**，这样，代码中的delete语句才能正确的释放本内存空间。
  由于找到的堆结点的大小不一定正好等于申请的大小，系统会自动的将多 余的那部分重新放入空闲链表中。

## C/C++为什么比python语言执行速度快？

一般来说一门语言的执行过程是：词法分析器将语言分词，语法分析器建立语法树，语义分析器按顺序遍历语法树并产生什么自定义的中间语言，以上内容称为编译器前端。接下来是生成汇编代码和代码优化，称为编译器后端。

目前编译器前端的方法还算成熟和通用，各语言也不在这一块区分运行速度。**重点是编译器后端。**

Python相比于C家族（C，C++，Java）这种现在可以算传统的语言，不同点一是：**生成字节码交由解释器执行**；二是：**一切皆class的弱类型**。这导致它不可能采用和C类似的后端。所以问题出在后端上。对于同一个算法，一方面语言性质上的不同使得编译产生的汇编结果也不同，从而导致速度上的区分，**像Python这种本身运行于解释器的语言再经过一层解释器转汇编很可能会拖慢运行速度**；另一方面，优化策略也会影响执行速度。



## 交叉编译

交叉编译可以理解为，**在当前编译平台下，编译出来的程序能运行在体系结构不同的另一种目标平台上**，但是编译平台本身却不能运行该程序：比如，我们在 x86 平台上，编写程序并编译成能运行在 ARM 平台的程序，编译得到的程序在 x86 平台上是不能运行的，必须放到 ARM 平台上才能运行。

之所以要有交叉编译，主要原因是：

Speed： 目标平台的运行速度往往比主机慢得多，**许多专用的嵌入式硬件被设计为低成本和低功耗，没有太高的性能**
Capability： **整个编译过程是非常消耗资源的，嵌入式系统往往没有足够的内存或磁盘空间**
Availability： 即使目标平台资源很充足，可以本地编译，但是第一个在目标平台上运行的本地编译器总需要通过交叉编译获得
Flexibility： 一个完整的Linux编译环境需要很多支持包，交叉编译使我们不需要花时间将各种支持包移植到目标板上



## CMake和Make编译

当你的程序只有一个源文件时，直接就可以用gcc命令编译它。但是当你的程序包含很多个源文件时，用gcc命令逐个去编译时，你就很容易混乱而且工作量大。

**makefile**是定义了整个项目工程的编译规则——“自动化编译”；

**make**是一个命令工具，是一个解释makefile中指令的工具。

**CMake**是一个跨平台的软件，能够输出各种各样的makefile或者project文件。

![这里写图片描述](MyNotes.assets/f57298cefba18fca5f31b8762f01d933_r.jpg)![img](MyNotes.assets/20190521175203875.png)



## 面向对象三大特性和五大原则

![img](MyNotes.assets/780561-20160808010837481-1065861963.jpg)

**三大基本特性：封装，继承，多态**

**五大基本原则：SPR, OCP, LSP, DIP, ISP**

- 单一职责原则SRP(Single Responsibility Principle)

  是指一个类的功能要单一，不能包罗万象。如同一个人一样，分配的工作不能太多，否则一天到晚虽然忙忙碌碌的，但效率却高不起来。

- 开放封闭原则OCP(Open－Close Principle)

  一个模块在扩展性方面应该是开放的而在更改性方面应该是封闭的。比如：一个网络模块，原来只服务端功能，而现在要加入客户端功能，那么应当在不用修改服务端功能代码的前提下，就能够增加客户端功能的实现代码，这要求在设计之初，就应当将服务端和客户端分开，公共部分抽象出来。

- 里式替换原则LSP(the Liskov Substitution Principle LSP)

  子类应当可以替换父类并出现在父类能够出现的任何地方。比如：公司搞年度晚会，所有员工可以参加抽奖，那么不管是老员工还是新员工，也不管是总部员工还是外派员工，都应当可以参加抽奖，否则这公司就不和谐了。

- 依赖倒置原则DIP(the Dependency Inversion Principle DIP)

  具体依赖抽象，上层依赖下层。假设B是较A低的模块，但B需要使用到A的功能，这个时候，B不应当直接使用A中的具体类： 而应当由B定义一抽象接口，并由A来实现这个抽象接口，B只使用这个抽象接口：这样就达到了依赖倒置的目的，B也解除了对A的依赖，反过来是A依赖于B定义的抽象接口。通过上层模块难以避免依赖下层模块，假如B也直接依赖A的实现，那么就可能 造成循环依赖。一个常见的问题就是编译A模块时需要直接包含到B模块的cpp文件，而编译B时同样要直接包含到A的cpp文件。

- 接口分离原则ISP(the Interface Segregation Principle ISP)

  模块间要通过抽象接口隔离开，而不是通过具体的类强耦合起来

## 目标文件

目标文件中的内容**包含编译后的机器指令代码，数据。还包括了链接时所需要的一些信息，比如符号表，调试信息，字符串等。**
目标文件将这些信息按不同的属性，以“**节（Section）**”的形式存储，有时候也叫做“**段（Segment）**”，它们表示一个一定长度的区域。

![img](MyNotes.assets/1023733-424e5974a3181efd.png)

（1）代码段（.text）
 程序源代码编译后的机器指令

（2）数据段（.data）
 已初始化了的全局变量和局部静态变量数据

（3）.bss段（.bss）
 未初始化的全局变量和局部静态变量

**注：**
 未初始化的全局变量和局部静态变量默认值都为0，本来也可以被放在.data段，但是在目标文件中为它们分配空间是没有必要的。因此，统一放到.bss段，.bss段大小为0，不出现在目标文件中。**而变量名以及变量的大小，与其他变量一样，放在了符号表（.symtab段）中。**

（4）其他段
 除了.text，.data，.bss这3个最常用的段之外，目标文件也有可能包含其他的段，用来保存与程序相关的其他信息。

例如：.symtab符号表，.debug调试信息，.dynamic动态链接信息，等等。



## C++11 新特性

- 1、新增**基于范围的for循环**：类似Java中foreach语句，为遍历数组提供了很大方便。 

  int nArr[5] = {1,2,3,4,5};for(int &x : nArr){x *=2;   //数组中每个元素倍乘}

- 2、**自动类型推断 auto**：它的作用就是当编译器在一个变量声明的时候，能够根据变量赋的值推断该变量的数据类型。这样就有些逼近Python中定义变量的功能，无需提前声明定义的变量的数据类型。

- 3、**匿名函数 Lambda**：如果代码里面存在大量的小函数，而这些函数一般只被一两处调用，那么不妨将它们重构成Lambda表达式，也就是匿名函数。作用就是当你想用一个函数，但是又不想费神去命名一个函数。

- 4、后置返回类型（tailng-return-type）

  template Ret adding_func(const Lhs &lhs, const Rhs &rhs) {return lhs + rhs;}

  这不是合法的C++，因为lhs和rhs还没定义；解析器解析完函数原型的剩余部分之前,它们还不是有效的标识符。为此, C++11引入了一种新的函数声明语法，叫做后置返回类型(trailing-return-type)。

- 5、**右值引用，移动语义**

- 6、**lambda**

- 7、**智能指针、空指针nullptr**



# STL(Standrd Template Library)

![这里写图片描述](MyNotes.assets/20180617100125575)

## **STL容器**

### **1、vector\向量**

 **(1)vector的底层结构**

vector底层是一个动态数组，包含三个迭代器，start和finish之间是已经被使用的空间范围，end_of_storage是整块连续空间包括备用空间的尾部。当空间不够装下数据（vec.push_back(val)）时，会自动申请另一片更大的空间（1.5倍或者2倍），然后把原来的数据拷贝到新的内存空间，接着释放原来的那片空间【vector内存增长机制】。当释放或者删除（vec.clear()）里面的数据时，其存储空间不释放，仅仅是清空了里面的数据。**因此，对vector的任何操作一旦引起了空间的重新配置，指向原vector的所有迭代器会都失效了。**

**（2）vector中的reserve和resize的区别**

**reserve扩大capacity的大小，resize扩大size的大小（随便填充数据）。**

reserve是直接扩充到已经确定的大小，可以减少多次开辟、释放空间的问题（优化push_back），就可以提高效率，其次还可以减少多次要拷贝数据的问题。reserve只是保证vector中的空间大小（capacity）最少达到参数所指定的大小n。reserve()只有一个参数。

resize()可以改变有效空间的大小，也有改变默认值的功能。capacity的大小也会随着改变。resize()可以有多个参数。

resize 可以改变 size 的大小，变大等价于调用 push_back(),变小等于 pop_back() ，resize 可以使capacity 的大小变大可以，但是不能让他变小。reserve 只可变大 vector 的 capacity 而不可以使其减小。不能改变size的大小。

```C++
vector<int> vec1{1};
vec1.resize(2);
for (auto v:vec1){
    cout << v << endl;
}
vector<int> vec2{1};
vec2.reserve(2);
for (auto v:vec2){
    cout << v << endl;
}
/*output:
1
0
1
*/
```

**（3）vector中的size和capacity的区别**

 **size表示当前vector中有多少个元素**（**finish - start**），而capacity函数则表示它已经分配的内存中**可以容纳多少元素**（**end_of_storage - start**）。

**（4）vector的元素类型可以是引用吗？**

  vector的底层实现要求连续的**对象排列**，**引用并非对象，没有实际地址，因此vector的元素类型不能是引用**。

**（5）正确释放vector的内存(clear(), swap(), shrink_to_fit())**  vec.clear()：清空内容，但是不释放内存。

  vector<int>().swap(vec)：清空内容，且释放内存，想得到一个全新的vector。

  vec.shrink_to_fit()：请求容器降低其capacity和size匹配。

  vec.clear();vec.shrink_to_fit();：清空内容，且释放内存。

**（6）vector 扩容为什么要以1.5倍或者2倍扩容?**  根据查阅的资料显示，考虑可能产生的堆空间浪费，成倍增长倍数不能太大，使用较为广泛的扩容方式有两种，以2倍的方式扩容，或者以1.5倍的方式扩容。 **（7）vector的常用函数**

```javascript
vector<int> vec(10,100);        创建10个元素,每个元素值为100
vec.resize(r,vector<int>(c,0)); 二维数组初始化
reverse(vec.begin(),vec.end())  将元素翻转
sort(vec.begin(),vec.end());    排序，默认升序排列
vec.push_back(val);             尾部插入数字
vec.size();                     向量大小
find(vec.begin(),vec.end(),1);  查找元素
iterator = vec.erase(iterator)  删除元素
```

### **2、list\列表**

**（1）list的底层原理**

  list的底层是一个**双向链表**，以结点为单位存放数据，结点的地址在内存中不一定连续，每次插入或删除一个元素，就配置或释放一个元素空间。

  list不支持随机存取，**如果需要大量的插入和删除**，而不关心随即存取

**（2）list的常用函数**

```javascript
list.push_back(elem)    在尾部加入一个数据
list.pop_back()            删除尾部数据
list.push_front(elem)    在头部插入一个数据
list.pop_front()        删除头部数据
list.size()                返回容器中实际数据的个数
list.sort()             排序，默认由小到大 
list.unique()           移除数值相同的连续元素
list.back()             取尾部迭代器
list.erase(iterator)    删除一个元素，参数是迭代器，返回的是删除迭代器的下一个位置
```

### **3、queue\队列**

queue、stack、priority_queue都是用deque适配器适配出来的

```javascript
qi.empty()
qi.front();//用front
qi.pop();
```

### **4、stack\栈**

```javascript
si.empty()
si.top();//使用top
si.pop();//pop数据
```

### **5、priority_queue\优先级队列**

**（1）priority_queue的底层原理**

  priority_queue：优先队列，其底层是用堆来实现的。在优先队列中，队首元素一定是当前队列中优先级最高的那一个。

**（2）priority_queue的常用函数**

```javascript
priority_queue<int, vector<int>, greater<int>> pq;   最小堆
priority_queue<int, vector<int>, less<int>> pq;      最大堆
pq.empty()   如果队列为空返回真
pq.pop()     删除对顶元素
pq.push(val) 加入一个元素
pq.size()    返回优先队列中拥有的元素个数
pq.top()     返回优先级最高的元素
```

### **6、set、multiset\集合**

**（1）map和set的异同？**

1.MAP的节点是一对数据.SET的节点是一个数据.
2.都属于stl中的关联容器

3.	map的形式 `map<type1, type2> mymap;`
      	set的形式 `set<type> myset;`

**（2）multiset和set和priority_queue区别**

**优先级队列只允许按照排序顺序访问一个元素（本质上仍是队列）** - 即，可以获得最高优先级的项目，而当删除该项目时，可以获得下一个最高优先级，依此类推。 **优先级队列还允许重复元素，因此它更像是一个multiset而不是set .**

一个**set允许以排序顺序进行完全访问**，例如，可以在集合的中间某处找到两个元素，然后按照从一个到另一个的顺序遍历。

```C++
//建堆(大顶堆为例)
//堆中有重复元素
//使用priority_queue:
priority_queue<int> pq; // 默认大顶堆, 小顶堆: priority_queue<int, vector<int>, greater<int>> pq;
pq.push(1); pq.push(2), ... // 添加元素
while(k-- && !pq.empty()) // 取出第k大的元素，堆内元素会减少
    res = pq.top(), pq.pop();

//使用multiset:
multiset<int, greater<int>> ms; // 默认是小根堆，所以需要加greater<int>
ms.insert(1), ms.insert(2), ... // 添加元素
// 取出第k大的数，堆内元素不会减少
for(auto i=ms.begin(); k-- && i!=ms.end(); ++i)
    res = *i;
```



### **7、map、multimap**

**（1）map 、set、multiset、multimap的底层原理**  map 、set、multiset、multimap的底层实现都是红黑树，epoll模型的底层数据结构也是红黑树，linux系统中CFS进程调度算法，也用到红黑树。

![image-20210327220457670](MyNotes.assets/image-20210327220457670.png)

红黑树的特性：

1. 每个结点或是红色或是黑色；
2. 根结点是黑色；
3. 每个叶结点是黑的；
4. 如果一个结点是红的，则它的两个儿子均是黑色；
5. 每个结点到其子孙结点的所有路径上包含相同数目的黑色结点。

对于STL里的map容器，count方法与find方法，都可以用来判断一个key是否出现，mp.count(key) > 0统计的是key出现的次数，因此只能为0/1，而mp.find(key) != mp.end()则表示key存在。

**（2）map 、set、multiset、multimap的特点**  set和multiset会根据特定的排序准则自动将元素排序，set中元素不允许重复，multiset可以重复。

map和multimap将key和value组成的pair作为元素，根据key的排序准则自动将元素排序（因为红黑树也是二叉搜索树，所以map默认是按key排序的），map中元素的key不允许重复，multimap可以重复。

map和set的增删改查速度为都是logn，是比较高效的。

**（3）为何map和set的插入删除效率比其他序列容器高，而且每次insert之后，以前保存的iterator不会失效？**

因为存储的是结点，不需要内存拷贝和内存移动。

因为插入操作只是结点指针换来换去，结点内存没有改变。而iterator就像指向结点的指针，内存没变，指向内存的指针也不会变。

**（4）为何map和set不能像vector一样有个reserve函数来预分配数据?**  

因为**在map和set内部存储的已经不是元素本身了，而是包含元素的结点**。也就是说map内部使用的Alloc并不是map<Key, Data, Compare, Alloc>声明的时候从参数中传入的Alloc。

**（5）map 、set、multiset、multimap的常用函数**

```javascript
it map.begin() 　        返回指向容器起始位置的迭代器（iterator） 
it map.end()             返回指向容器末尾位置的迭代器 
bool map.empty()         若容器为空，则返回true，否则false
it map.find(k)           寻找键值为k的元素，并用返回其地址
int map.size()           返回map中已存在元素的数量
map.insert({int,string}) 插入元素
for (itor = map.begin(); itor != map.end();)
{
    if (itor->second == "target")
        map.erase(itor++) ; // erase之后，令当前迭代器指向其后继。
    else
        ++itor;
}
```



### **8、unordered_map**

**（1）unordered_map、unordered_set的底层原理**

 C++ 11中对unordered_set描述大体如下：无序集合容器**（unordered_set）是一个存储唯一(unique，即无重复）的关联容器**（Associative container），容器中的元素无特别的秩序关系，该容器允许基于值的快速元素检索，同时也支持正向迭代。
       在一个unordered_set内部，元素不会按任何顺序排序，而是通过元素值的hash值将元素分组放置到各个槽(Bucker，也可以译为“桶”），这样就能通过元素值快速访问各个对应的元素（均摊耗时为O（1））。

**unordered_map的底层是一个防冗余的哈希表（采用除留余数法）**。哈希表最大的优点，就是把数据的存储和查找消耗的时间大大降低，时间复杂度为O(1)；而代价仅仅是消耗比较多的内存。

使用一个下标范围比较大的数组来存储元素。可以设计一个函数（哈希函数（一般使用除留取余法），也叫做散列函数），使得每个元素的key都与一个函数值（即数组下标，hash值）相对应，于是用这个数组单元来存储这个元素；也可以简单的理解为，按照key为每一个元素“分类”，然后将这个元素存储在相应“类”所对应的地方，称为桶。

但是，不能够保证每个元素的key与函数值是一一对应的，因此极有可能出现对于不同的元素，却计算出了相同的函数值，这样就产生了“冲突”，换句话说，就是把不同的元素分在了相同的“类”之中。 一般可采用拉链法解决冲突：

**（2）unordered_map 与map的区别？使用场景？** 

构造函数：unordered_map 需要hash函数，等于函数;map只需要比较函数(小于函数). 
存储结构：**unordered_map 采用hash表存储，map一般采用红黑树(RB Tree) 实现**。因此其memory数据结构是不一样的。

总体来说，**unordered_map 查找速度会比map快**，而且查找速度基本和数据数据量大小，属于常数级别;而map的查找速度是log(n)级别。并不一定常数就比log(n)小，hash还有hash函数的耗时，明白了吧，如果你考虑效率，特别是在元素达到一定数量级时，考虑考虑unordered_map 。但若你对内存使用特别严格，希望程序尽可能少消耗内存，那么一定要小心，unordered_map 可能会让你陷入尴尬，特别是当你的unordered_map 对象特别多时，你就更无法控制了，而且**unordered_map 的构造速度较慢**。

**（4）unordered_map、unordered_set的常用函数**

```javascript
unordered_map.begin() 　　  返回指向容器起始位置的迭代器（iterator） 
unordered_map.end() 　　    返回指向容器末尾位置的迭代器 
unordered_map.cbegin()　    返回指向容器起始位置的常迭代器（const_iterator） 
unordered_map.cend() 　　   返回指向容器末尾位置的常迭代器 
unordered_map.size()  　　  返回有效元素个数 
unordered_map.insert(key)  插入元素 
unordered_map.find(key) 　 查找元素，返回迭代器
unordered_map.count(key) 　返回匹配给定主键的元素的个数 
```

### **9、algorithm**

**find** 用法：find(first, end, value);

返回区间[first，end）中第一个值等于value的元素位置；若未找到，返回end。函数返回的是迭代器或指针，即位置信息。 String是这一种顺序存储结构，其find函数返回的是下标索引。set,map,multiset,multimap都不是顺序索引的数据结构，所以返回的是迭代器。 vector没有实现find函数，除此之外，常见容器都实现了自己的find函数。对于string，通过a.find(value)==string::npos判断 **count** count函数的功能是：统计容器中等于value元素的个数。

先看一下函数的参数： count(first,last,value); first是容器的首迭代器，last是容器的末迭代器，value是询问的元素。返回这个值出现次数的统计结果。 **remove_if** remove_if(beg, end, op)   //移除区间[beg,end)中每一个“令判断式:op(elem)获得true”的元素； 　remove_if(remove和unique也是相同情况)的参数是迭代器，通过迭代器无法得到容器本身，而要删除容器内的元素只能通过容器的成员函 数来进行，因此remove系列函数无法真正删除元素，只能把要删除的元素移到容器末尾并返回要被删除元素的迭代器，然后通过erase成员函数来真正删除。用法如下：

bool IsSpace(char x) { return x == ' '; }

string str2 = "Text with some  spaces"; str2.erase(remove_if(str2.begin(),  str2.end(), IsSpace), str2.end()); // "Textwithsomespaces" **count_if** count_if(first,last,comp) (在comp为true的情况下计数） 或者 count_if(first,last,value,comp) (这个是在comp为true的情况下统计容器中等于value的元素）：first为首迭代器，last为末迭代器，value为要查询的元素，comp为比较bool函数，为true则计数，函数返回型是int。

**max** 传两个参数取最大值 **min** 传两个参数取最小值 **lower_bound** **merge** ，merge函数的作用是：将两个有序的序列合并为一个有序的序列。函数参数：merge(first1,last1,first2,last2,result,compare);

//firs1t为第一个容器的首迭代器，last1为第一个容器的末迭代器，first2为第二个容器的首迭代器，last2为容器的末迭代器，result为存放结果的容器，comapre为比较函数（可略写，默认为合并为一个升序序列）。

**remove** remove(begin,end,const T& value)  //移除区间{beg,end)中每一个“与value相等”的元素； 　remove只是通过迭代器的指针向前移动来删除，将没有被删除的元素放在链表的前面，并返回一个指向新的超尾值的迭代器。由于remove()函数不是成员，因此不能调整链表的长度。remove()函数并不是真正的删除，要想真正删除元素则可以使用erase()或者resize()函数。 string str1 = "Text with some   spaces"; str1.erase(std::remove(str1.begin(), str1.end(), ' '),  str1.end());  // "Textwithsomespaces" **unique** unique是STL比较实用的一个函数。用于“去除”容器内相邻的重复的元素（只保留一个）。这里说的去除并不是真正将容器内的重复元素删去，只是把重复的元素移到容器最后，但是依然在容器内。 对于数组而言返回去重后最后一个元素的指针，而其他容器则是返回去重后最后一个元素的迭代器。 **reverse** 函数参数：reverse(first，last)，其中first，last分别指向被反转序列中初始及末尾位置的双向迭代器（Bidirectional iterators）。这个范围即 [first,last) ，包括 first 到 last 间的所有元素，包括 first 指向的元素，但不包括 last 指向的元素。 **sort** sort(vi.begin(),vi.end());默认大序 
**substr** substr有2种用法： 假设：string s = "0123456789"; string sub1 = s.substr(5); //只有一个数字5表示从下标为5开始一直到结尾：sub1 = "56789" string sub2 = s.substr(5, 3); //从下标为5开始截取长度为3位：sub2 = "567"

**erase** vector中真正去除重复元素的方法，先排序，然后使用vi.erase(unique(vi.begin(),vi.end()),vi.end())



## array和vector，数组三者区别和联系

- 共同点
  （1.）都和数组相似，都可以使用标准数组的表示方法来访问每个元素（array和vector都对下标运算符[ ]进行了重载）
  **（2.）三者的存储都是连续的，可以进行随机访问**
- 不同点
  **（0.）数组是不安全的，array和vector是比较安全的（有效的避免越界等问题）**
  （1.）array对象和数组存储在相同的内存区域（栈）中，vector对象存储在自由存储区（堆）
  （2.）array可以将一个对象赋值给另一个array对象，但是数组不行
  **（3.）vector属于变长的容器，即可以根据数据的插入和删除重新构造容器容量；但是array和数组属于定长容器**
  （4.）vector和array提供了更好的数据访问机制，即可以使用front()和back()以及at()（at()可以避免a[-1]访问越界的问题）访问方式，使得访问更加安全。而数组只能通过下标访问，在写程序中很容易出现越界的错误
  （5.）vector和array提供了更好的遍历机制，即有正向迭代器和反向迭代器
  （6.）vector和array提供了size()和Empty()，而数组只能通过sizeof()/strlen()以及遍历计数来获取大小和是否为空
  （7.）vector和array提供了两个容器对象的内容交换，即swap()的机制，而数组对于交换只能通过遍历的方式逐个交换元素
  （8.）array提供了初始化所有成员的方法fill（）
  （9.）由于vector的动态内存变化的机制，在插入和删除时，需要考虑迭代的是否有效问题
  （10.）vector和array在声明变量后，在声明周期完成后，会自动地释放其所占用的内存。对于数组如果用new[ ]/malloc申请的空间，必须用对应的delete[ ]和free来释放内存



# Python

## bisect

> https://docs.python.org/zh-cn/3.7/library/bisect.html

[`bisect `](https://docs.python.org/zh-cn/3.7/library/bisect.html#module-bisect)使用了基本的二分（bisection）算法，**默认传入列表是有序的**。源代码也可以作为很棒的算法示例（边界判断也做好啦！）

定义了以下函数：

- `bisect.bisect_left`(*a*, *x*, *lo=0*, *hi=len(a)*)[¶](https://docs.python.org/zh-cn/3.7/library/bisect.html#bisect.bisect_left)

  在 *a* 中找到 *x* 合适的插入点以维持有序，**寻找插入位置**。参数 *lo* 和 *hi* 可以被用于确定需要考虑的子集；默认情况下整个列表都会被使用。如果 *x* 已经在 *a* 里存在，那么插入点会在已存在元素之前（也就是左边）。如果 *a* 是列表（list）的话，返回值是可以被放在 `list.insert()` 的第一个参数的。返回的插入点 *i* 可以将数组 *a* 分成两部分。左侧是 `all(val < x for val in a[lo:i])` ，右侧是 `all(val >= x for val in a[i:hi])` 。

- `bisect.bisect_right`(*a*, *x*, *lo=0*, *hi=len(a)*)

- `bisect.bisect`(*a*, *x*, *lo=0*, *hi=len(a)*)

  类似于 [`bisect_left()`](https://docs.python.org/zh-cn/3.7/library/bisect.html#bisect.bisect_left)，但是返回的插入点是 *a* 中已存在元素 *x* 的右侧。返回的插入点 *i* 可以将数组 *a* 分成两部分。左侧是 `all(val <= x for val in a[lo:i])`，右侧是 `all(val > x for val in a[i:hi])` for the right side。

- `bisect.insort_left`(*a*, *x*, *lo=0*, *hi=len(a)*)

  将 *x* **插入**到一个有序序列 *a* 里，并维持其有序。如果 *a* 有序的话，这相当于 `a.insert(bisect.bisect_left(a, x, lo, hi), x)`。要注意**搜索是 O(logn) 的，插入却是 O(n) 的**。

- `bisect.insort_right`(*a*, *x*, *lo=0*, *hi=len(a)*)

- `bisect.insort`(*a*, *x*, *lo=0*, *hi=len(a)*)

  类似于 [`insort_left()`](https://docs.python.org/zh-cn/3.7/library/bisect.html#bisect.insort_left)，但是把 *x* 插入到 *a* 中已存在元素 *x* 的右侧。

## heapq

> https://docs.python.org/zh-cn/3.7/library/heapq.html

- `heapq.heappush`(*heap*, *item*)[¶](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappush)

  将 *item* 的值加入 *heap* 中，保持堆的不变性。

- `heapq.heappop`(*heap*)

  弹出并返回 *heap* 的最小的元素，保持堆的不变性。如果堆为空，抛出 [`IndexError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#IndexError) 。使用 `heap[0]` ，可以只访问最小的元素而不弹出它。

- `heapq.heappushpop`(*heap*, *item*)

  将 *item* 放入堆中，然后弹出并返回 *heap* 的最小元素。该组合操作比先调用 [`heappush()`](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappush) 再调用 [`heappop()`](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappop) 运行起来更有效率。

- `heapq.heapify`(*x*)

  将list *x* 转换成堆，原地，线性时间内。

- `heapq.heapreplace`(*heap*, *item*)

  弹出并返回 *heap* 中最小的一项，同时推入新的 *item*。 堆的大小不变。 如果堆为空则引发 [`IndexError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#IndexError)。这个单步骤操作比 [`heappop()`](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappop) 加 [`heappush()`](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappush) 更高效，并且在使用固定大小的堆时更为适宜。 pop/push 组合总是会从堆中返回一个元素并将其替换为 *item*。返回的值可能会比添加的 *item* 更大。 如果不希望如此，可考虑改用 [`heappushpop()`](https://docs.python.org/zh-cn/3.7/library/heapq.html#heapq.heappushpop)。 它的 push/pop 组合会返回两个值中较小的一个，将较大的值留在堆中。

该模块还提供了三个基于堆的通用功能函数。

- `heapq.merge`(**iterables*, *key=None*, *reverse=False*)

  将多个已排序的输入合并为一个已排序的输出（例如，合并来自多个日志文件的带时间戳的条目）。 返回已排序值的 [iterator](https://docs.python.org/zh-cn/3.7/glossary.html#term-iterator)。类似于 `sorted(itertools.chain(*iterables))` 但返回一个可迭代对象，不会一次性地将数据全部放入内存，并假定每个输入流都是已排序的（从小到大）。具有两个可选参数，它们都必须指定为关键字参数。*key* 指定带有单个参数的 [key function](https://docs.python.org/zh-cn/3.7/glossary.html#term-key-function)，用于从每个输入元素中提取比较键。 默认值为 `None` (直接比较元素)。*reverse* 为一个布尔值。 如果设为 `True`，则输入元素将按比较结果逆序进行合并。 要达成与 `sorted(itertools.chain(*iterables), reverse=True)` 类似的行为，所有可迭代对象必须是已从大到小排序的。*在 3.5 版更改:* 添加了可选的 *key* 和 *reverse* 形参。

- `heapq.nlargest`(*n*, *iterable*, *key=None*)

  从 *iterable* 所定义的数据集中返回前 *n* 个最大的元素。 如果提供了 *key* 则其应指定一个单参数的函数，用于从 that is used to extract a comparison key from each element in *iterable* 的每个元素中提取比较键 (例如 `key=str.lower`)。 等价于: `sorted(iterable, key=key, reverse=True)[:n]`。

- `heapq.nsmallest`(*n*, *iterable*, *key=None*)

  从 *iterable* 所定义的数据集中返回前 *n* 个最小元素组成的列表。 如果提供了 *key* 则其应指定一个单参数的函数，用于从 *iterable* 的每个元素中提取比较键 (例如 `key=str.lower`)。 等价于: `sorted(iterable, key=key)[:n]`。

后两个函数在 *n* 值较小时性能最好。 对于更大的值，使用 [`sorted()`](https://docs.python.org/zh-cn/3.7/library/functions.html#sorted) 函数会更有效率。 此外，当 `n==1` 时，使用内置的 [`min()`](https://docs.python.org/zh-cn/3.7/library/functions.html#min) 和 [`max()`](https://docs.python.org/zh-cn/3.7/library/functions.html#max) 函数会更有效率。 如果需要重复使用这些函数，请考虑将可迭代对象转为真正的堆。

```python
>>> def heapsort(iterable):
...     h = []
...     for value in iterable:
...         heappush(h, value)
...     return [heappop(h) for i in range(len(h))]
...
>>> heapsort([1, 3, 5, 7, 9, 2, 4, 6, 8, 0])
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```



## collections

> https://docs.python.org/zh-cn/3.7/library/collections.html

### deque对象

- *class* `collections.deque`([*iterable*[, *maxlen*]])

  返回一个新的双向队列对象，从左到右初始化(用方法 [`append()`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.deque.append)) ，从 *iterable* （迭代对象) 数据创建。如果 *iterable* 没有指定，新队列为空。

  双向队列(deque)对象支持以下方法：

  - `append`(*x*)

    添加 *x* 到右端。

  - `appendleft`(*x*)

    添加 *x* 到左端。

  - `clear`()

    移除所有元素，使其长度为0.

  - `copy`()

    创建一份浅拷贝。*3.5 新版功能.*

  - `count`(*x*)

    计算 deque 中元素等于 *x* 的个数。*3.2 新版功能.*

  - `extend`(*iterable*)

    扩展deque的右侧，通过添加iterable参数中的元素。

  - `extendleft`(*iterable*)

    扩展deque的左侧，通过添加iterable参数中的元素。注意，左添加时，在结果中iterable参数中的顺序将被反过来添加。

  - `index`(*x*[, *start*[, *stop*]])

    返回 *x* 在 deque 中的位置（在索引 *start* 之后，索引 *stop* 之前）。 返回第一个匹配项，如果未找到则引发 [`ValueError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#ValueError)。*3.5 新版功能.*

  - `insert`(*i*, *x*)

    在位置 *i* 插入 *x* 。如果插入会导致一个限长 deque 超出长度 *maxlen* 的话，就引发一个 [`IndexError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#IndexError)。*3.5 新版功能.*

  - `pop`()

    移去并且返回一个元素，deque 最右侧的那一个。 如果没有元素的话，就引发一个 [`IndexError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#IndexError)。

  - `popleft`()

    移去并且返回一个元素，deque 最左侧的那一个。 如果没有元素的话，就引发 [`IndexError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#IndexError)。

  - `remove`(*value*)

    移除找到的第一个 *value*。 如果没有的话就引发 [`ValueError`](https://docs.python.org/zh-cn/3.7/library/exceptions.html#ValueError)。

  - `reverse`()

    将deque逆序排列。返回 `None` 。*3.2 新版功能.*

  - `rotate`(*n=1*)

    向右循环移动 *n* 步。 如果 *n* 是负数，就向左循环。如果deque不是空的，向右循环移动一步就等价于 `d.appendleft(d.pop())` ， 向左循环一步就等价于 `d.append(d.popleft())` 。

  Deque对象同样提供了一个只读属性:

  - `maxlen`

    Deque的最大尺寸，如果没有限定的话就是 `None` 。

### Counter对象

一个 [`Counter`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.Counter) 是一个 [`dict`](https://docs.python.org/zh-cn/3.7/library/stdtypes.html#dict) 的子类，用于计数可哈希对象。

- *class* `collections.``Counter`([*iterable-or-mapping*]) 

  - `elements`()

    返回一个迭代器，每个元素重复计数的个数。元素顺序是任意的。如果一个元素的计数小于1， [`elements()`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.Counter.elements) 就会忽略它。`>>> c = Counter(a=4, b=2, c=0, d=-2) >>> sorted(c.elements()) ['a', 'a', 'a', 'a', 'b', 'b'] `

  - `most_common`([*n*])

    返回一个列表，提供 *n* 个频率最高的元素和计数。 如果没提供 *n* ，或者是 `None` ， [`most_common()`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.Counter.most_common) 返回计数器中的 *所有* 元素。相等个数的元素顺序随机：`>>> Counter('abracadabra').most_common(3)   [('a', 5), ('r', 2), ('b', 2)] `

  - `subtract`([*iterable-or-mapping*])

    从 *迭代对象* 或 *映射对象* 减去元素。像 [`dict.update()`](https://docs.python.org/zh-cn/3.7/library/stdtypes.html#dict.update) 但是是减去，而不是替换。输入和输出都可以是0或者负数。`>>> c = Counter(a=4, b=2, c=0, d=-2) >>> d = Counter(a=1, b=2, c=3, d=4) >>> c.subtract(d) >>> c Counter({'a': 3, 'b': 0, 'c': -3, 'd': -6}) `

  通常字典方法都可用于 [`Counter`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.Counter) 对象，除了有两个方法工作方式与字典并不相同。

  - `fromkeys`(*iterable*)

    这个类方法没有在 [`Counter`](https://docs.python.org/zh-cn/3.7/library/collections.html#collections.Counter) 中实现。

  - `update`([*iterable-or-mapping*])

    从 *迭代对象* 计数元素或者 从另一个 *映射对象* (或计数器) 添加。 像 [`dict.update()`](https://docs.python.org/zh-cn/3.7/library/stdtypes.html#dict.update) 但是是加上，而不是替换。另外，*迭代对象* 应该是序列元素，而不是一个 `(key, value)` 对。

## list

> https://blog.csdn.net/qq_45949008/article/details/103375664
>
> https://blog.csdn.net/Baoli1008/article/details/48059623

**list底层实现类似动态数组**，当底层数组容量满了而需要扩充的时候，python依据规则会扩充多个位置出来。比如初始化列表array=[1, 2, 3, 4]，向其中添加元素23，此时array对应的底层数组，扩充后的容量不是5，而是8。这就是over-allocate的意义，即扩充容量的时候会多分配一些存储空间。

| 操作 | 时间复杂度 |
| ---- | ---------- |
| index()|O(1)|
|append()	|O(1)|
|extend()	|O(k)|
|insert()	|O(n)|
|count()	|O(n)|
|remove()	|O(n)|
|pop()	|O(1)|
|pop(i)	|O(n)|
|**sort()**	|**O(nlogn)**|
|reverse()	|O(n)|
|len()	|O(1)|
|max(),min()	|O(n)|
|del(del list[i] 或者 del list[i:j])	|O(n)|
|slice [x:y] (切片)	|O(k)|
|iterration（列表迭代）	|O(n)|
|in 关键字	|O(n)|

通过分析可以发现，**列表不太适合做元素的遍历、删除、插入等操作**，对应的时间复杂度为O(n)；**访问某个索引的元素、尾部添加元素或删除元素这些操作比较适合做**，对应的时间复杂度为O(1)。

## sort和sorted

```python
list.sort(key=None, reverse=False)
#key -- 主要是用来进行比较的元素，只有一个参数，具体的函数的参数就是取自于可迭代对象中，指定可迭代对象中的一个元素来进行排序。
#reverse -- 排序规则，reverse = True 降序， reverse = False 升序（默认）。
```

1. **sort原位排序**，无返回值；sorted保留原对象，返回一个排序完的对象。
2. **sort只能对列表进行排序；sorted可以对任何可迭代对象iterable（字符串，列表，元组，字典等）进行排序**
3. **sorted返回新对象**，所以耗费较多资源。如果无需保存原对象，可以优先使用sort函数，节省内存空间。

## lambda

`格式为 lambda 形参名：返回值`

匿名函数`lambda x: x * x`实际上就是：

```python
def f(x):
    return x * x
```

关键字`lambda`表示匿名函数，冒号前面的`x`表示函数参数。

匿名函数有个限制，就是只能有一个表达式，不用写`return`，返回值就是该表达式的结果。

用匿名函数有个好处，因为函数没有名字，不必担心函数名冲突。此外，匿名函数也是一个函数对象，也可以把匿名函数赋值给一个变量，再利用变量来调用该函数：

```bash
>>> f = lambda x: x * x
>>> f
<function <lambda> at 0x101c6ef28>
>>> f(5)
25
```

# Computer Network

## 1、网络层次划分

OSI/RM模型（Open System Interconnection/Reference Model）。它将计算机网络体系结构的通信协议划分为七层，自下而上依次为：物理层（Physics Layer）、数据链路层（Data Link Layer）、网络层（Network Layer）、传输层（Transport Layer）、会话层（Session Layer）、表示层（Presentation Layer）、应用层（Application Layer）。

![img](MyNotes.assets/1538030296-7490-20150904094019903-1923900106.jpg)

![img](MyNotes.assets/1538030296-8668-20150904095142060-1017190812.gif)

**1）物理层（Physical Layer）**

激活、维持、关闭通信端点之间的机械特性、电气特性、功能特性以及过程特性。该层为上层协议提供了一个传输数据的可靠的物理媒体。简单的说，**物理层确保原始的数据可在各种物理媒体上传输。**物理层记住两个重要的设备名称，中继器（Repeater，也叫放大器）和集线器。

**2）数据链路层（Data Link Layer）**

数据链路层在物理层提供的服务的基础上向网络层提供服务，其最基本的服务是**将源自网络层来的数据可靠地传输到相邻节点的目标机网络层**。该层的作用包括：物理地址寻址、数据的成帧、流量控制、数据的检错、重发等。

有关数据链路层的重要知识点：

- **1> 数据链路层为网络层提供可靠的数据传输；**
- **2> 基本数据单位为帧；**
- **3> 主要的协议：以太网协议；**
- **4> 两个重要设备名称：网桥和交换机。**

**3）网络层（Network Layer）**

网络层的目的是实现两个端系统之间的数据透明传送，具体功能包括寻址和路由选择、连接的建立、保持和终止等。简而言之，即**"路径选择、路由及逻辑寻址"**。

网络层中涉及众多的协议，其中包括最重要的协议，也是TCP/IP的核心协议——IP协议。IP协议非常简单，仅仅提供不可靠、无连接的传送服务。IP协议的主要功能有：无连接数据报传输、数据报路由选择和差错控制。与IP协议配套使用实现其功能的还有地址解析协议ARP、逆地址解析协议RARP、因特网报文协议ICMP、因特网组管理协议IGMP。

有关网络层的重点为：

- **1> 网络层负责对子网间的数据包进行路由选择。此外，网络层还可以实现拥塞控制、网际互连等功能；**
- **2> 基本数据单位为IP数据报；**
- **3> 包含的主要协议：**
  - **IP协议（Internet Protocol，因特网互联协议）;**
  - **ICMP协议（Internet Control Message Protocol，因特网控制报文协议）;**
  - **ARP协议（Address Resolution Protocol，地址解析协议）;**
  - **RARP协议（Reverse Address Resolution Protocol，逆地址解析协议）。**
- **4> 重要的设备：路由器。**

**4）传输层（Transport Layer）**

传输层的任务是根据通信子网的特性，最佳的利用网络资源，为两个端系统的会话层之间，提供建立、维护和取消传输连接的功能，负责端到端的可靠数据传输。在这一层，信息传送的协议数据单元称为段或报文。 网络层只是根据网络地址将源结点发出的数据包传送到目的结点，而**传输层则负责将数据可靠地传送到相应的端口**。 有关网络层的重点：

有关网络层的重点：

- 1> 传输层负责将上层数据分段并提供端到端的、可靠的或不可靠的传输以及端到端的差错控制和流量控制问题；
- 2> 包含的主要协议：TCP协议（Transmission Control Protocol，传输控制协议）、UDP协议（User Datagram Protocol，用户数据报协议）；
- 3> 重要设备：网关。

**5）应用层**

为操作系统或网络应用程序提供访问网络服务的接口。

会话层、表示层和应用层重点：

- **1> 数据传输基本单位为报文；**
- **2> 包含的主要协议：FTP（文件传送协议）、Telnet（远程登录协议）、DNS（域名解析协议）、SMTP（邮件传送协议），POP3协议（邮局协议），HTTP协议（Hyper Text Transfer Protocol）。**

## 2、IP地址

> https://blog.csdn.net/qq_37236745/article/details/101868046?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-0&spm=1001.2101.3001.4242

**IP 地址 ::= { <网络号>, <主机号>}**

**一个网络号在整个因特网范围内是唯一的**。第二个字段是**主机号（host-id）**，它标志该主机（或路由器）。**一个主机号在它前面的网络号所指明的网络范围内是唯一的**。

![img](MyNotes.assets/aHR0cHM6Ly9zMS5heDF4LmNvbS8yMDE4LzExLzE4L2l6YkJNcS5qcGc)

**A、B、C类私有地址**

私有地址(private address)也叫专用地址，它们不会在全球使用，只具有本地意义。

A类私有地址：10.0.0.0/8，范围是：10.0.0.0~10.255.255.255

B类私有地址：172.16.0.0/12，范围是：172.16.0.0~172.31.255.255

C类私有地址：192.168.0.0/16，范围是：192.168.0.0~192.168.255.255

## 3、子网掩码及网络划分

随着互连网应用的不断扩大，**原先的IPv4的弊端也逐渐暴露出来，即网络号占位太多，而主机号位太少，所以其能提供的主机地址也越来越稀缺**，目前除了使用NAT在企业内部利用保留地址自行分配以外，通常都**对一个高类别的IP地址进行再划分，以形成多个子网，提供给不同规模的用户群使用。**

这里主要是**为了在网络分段情况下有效地利用IP地址，通过对主机号的高位部分取作为子网号**，从通常的网络位界限中扩展或压缩子网掩码，用来创建某类地址的更多子网。**但创建更多的子网时，在每个子网上的可用主机地址数目会比原先减少。**

- 将IP地址和子网掩码换算为二进制，子网掩码连续全1的是网络地址，后面的是主机地址，虚线前为网络地址，虚线后为主机地址
- **IP地址和子网掩码进行与运算，结果是网络地址**（即主机号全0是网络地址）
- 将运算结果中的网络地址不变，**主机地址变为1，结果就是广播地址**
- 地址范围就是含在本网段内的所有主机

### 示例
一个主机的IP地址是`202.112.14.137`，掩码是`255.255.255.224`，要求计算这个主机所在网络的网络地址和广播地址

根据子网掩码可以分割网络号+主机号
`255.255.255.224` 转二进制：

`11111111 11111111 11111111 11100000`

网络号有27位，主机号有5位

网络地址就是：把IP地址转成二进制和子网掩码进行**与运算**

`11001010 01110000 00001110 10001001`

IP地址&子网掩码

```text
11001010 01110000 00001110 10001001

11111111 11111111 11111111 11100000

------------------------------------------------------

11001010 01110000 00001110 10000000
即：202.112.14.128
```

计算**广播地址**
广播地址：网络地址的**主机位全部变成1** ，10011111 即159 即：202.112.14.159

主机数
主机号有5位，那么这个地址中，就只能有25−2=3025−2=30个主机

**因为其中全0作为网络地址，全1作为广播地址**



## 4、TCP/IP协议

**TCP负责发现传输的问题，一有问题就发出信号，要求重新传输，直到所有数据安全正确地传输到目的地。而IP是给因特网的每一台联网设备规定一个地址。**

![img](MyNotes.assets/1538030297-3779-20150904110054856-961661137.png)![img](MyNotes.assets/1538030297-7824-20150904110008388-1768388886.gif)

**为什么要三次握手？**

在只有两次"握手"的情形下，假设Client想跟Server建立连接，但是却因为中途连接请求的数据报丢失了，故Client端不得不重新发送一遍；这个时候Server端仅收到一个连接请求，因此可以正常的建立连接。但是，有时候Client端重新发送请求不是因为数据报丢失了，而是有可能数据传输过程因为网络并发量很大在某结点被阻塞了，这种情形下Server端将先后收到2次请求，并持续等待两个Client请求向他发送数据...问题就在这里，Cient端实际上只有一次请求，而Server端却有2个响应，极端的情况可能由于Client端多次重新发送请求数据而导致Server端最后建立了N多个响应在等待，因而造成极大的资源浪费！所以，"三次握手"很有必要！

**为什么要四次挥手？**

试想一下，假如现在你是客户端你想断开跟Server的所有连接该怎么做？第一步，你自己先停止向Server端发送数据，并等待Server的回复。但事情还没有完，虽然你自身不往Server发送数据了，但是因为你们之前已经建立好平等的连接了，所以此时他也有主动权向你发送数据；故Server端还得终止主动向你发送数据，并等待你的确认。其实，说白了就是保证双方的一个合约的完整执行！

使用TCP的协议：FTP（文件传输协议）、Telnet（远程登录协议）、SMTP（简单邮件传输协议）、POP3（和SMTP相对，用于接收邮件）、HTTP协议等。



## 5、UDP协议

**UDP用户数据报协议，是面向无连接的通讯协议**，UDP数据包括目的端口号和源端口号信息，**由于通讯不需要连接，所以可以实现广播发送。**

UDP主要用于那些面向查询---应答的服务，例如NFS。相对于FTP或Telnet，这些服务需要交换的信息量较小。

使用UDP协议包括：TFTP（简单文件传输协议）、SNMP（简单网络管理协议）、DNS（域名解析协议）、NFS、BOOTP。

### TCP与UDP的区别

![tcp-vs-udp](MyNotes.assets/tcp-vs-udp.jpg)

TCP (Transmission Control Protocol)和UDP(User Datagram Protocol)协议属于传输层协议

它们之间的区别包括：

TCP是面向连接的，UDP是无连接的；

TCP是可靠的，UDP是不可靠的；

TCP只支持点对点通信，UDP支持一对一、一对多、多对一、多对多的通信模式；

TCP首部开销(20个字节)比UDP的首部开销(8个字节)要大；

### TCP和UDP应用的例子

TCP应用：

- FTP：文件传输协议
- SSH：安全登录、文件传送（SCP）和端口重定向
- Telnet：不安全文本传送
- SMTP：简单邮件传输协议
- HTTP：超文本传输协议

UDP应用：

- 流媒体

  如果采用TCP，一旦发生丢包，TCP会将后续包缓存起来，等前面的包重传并接收到后再继续发送，延迟会越来越大。基于UDP的协议入WebRTC使极佳的选择。

- 实时游戏

  对实时要求较为严格的情况下，采用自定义的可靠UDP协议，自定义重传策略，能哦把丢包产生的延迟降到最低，尽量减少网络问题对游戏性造成的影响。

- 物联网

## 6、DNS协议

**将URL转换为IP地址**。域名是由圆点分开一串单词或缩写组成的，每一个域名都对应一个惟一的IP地址，在Internet上域名与IP地址之间是一一对应的，DNS就是进行域名解析的服务器。DNS命名用于Internet等TCP/IP网络中，通过用户友好的名称查找计算机和服务。



## 7、HTTP协议

超文本传输协议（**HTTP**，HyperText Transfer Protocol)是互联网上应用最为广泛的一种网络协议。所有的 WWW（万维网） 文件都必须遵守这个标准。**设计 HTTP 最初的目的是为了提供一种发布和接收 HTML 页面的方法。**

###  **Http和Https的区别**

Http协议运行在TCP之上，**明文传输**，客户端与服务器端都无法验证对方的身份；

Https是身披SSL(Secure Socket Layer)外壳的Http，运行于SSL上，SSL运行于TCP之上，是添加了**加密和认证机制**的HTTP。



## MAC地址

**Media Access Control Address**，直译为**媒体存取控制位址**，也称为**局域网地址**（LAN Address），**MAC位址**，**以太网地址**（Ethernet Address）或**物理地址**（Physical Address），它是一个用来确认网络设备位置的位址。

MAC地址则是48位的（6个字节），通常表示为12个16进制数，每2个16进制数之间用冒号隔开，如08：00：20：0A：8C：6D就是一个MAC地址。

### IP地址和MAC地址相同点是它们都唯一，不同的特点主要有：

1. 对于网络上的某一设备，如一台计算机或一台路由器，其IP地址是基于网络拓扑设计出的，同一台设备或计算机上，改动IP地址是很容易的（但必须唯一），而MAC则是生产厂商烧录好的，一般不能改动。我们可以根据需要给一台主机指定任意的IP地址，如我们可以给局域网上的某台计算机分配IP地址为192.168.0.112 ，也可以将它改成192.168.0.200。而任一网络设备（如网卡，路由器）一旦生产出来以后，其MAC地址不可由本地连接内的配置进行修改。如果一个计算机的网卡坏了，在更换网卡之后，该计算机的MAC地址就变了 [5] 。
2. 长度不同。IP地址为32位，MAC地址为48位  。
3. 分配依据不同。IP地址的分配是基于网络拓扑，MAC地址的分配是基于制造商。
4. 寻址协议层不同。IP地址应用于OSI第三层，即网络层，而MAC地址应用在OSI第二层，即数据链路层。 数据链路层协议可以使数据从一个节点传递到相同链路的另一个节点上（通过MAC地址），而网络层协议使数据可以从一个网络传递到另一个网络上（ARP根据目的IP地址，找到中间节点的MAC地址，通过中间节点传送，从而最终到达目的网络） 。

## ARP/RARP协议

**地址解析协议，即ARP（Address Resolution Protocol），是根据IP地址获取物理地址的一个TCP/IP协议。**

**ARP命令可用于查询本机ARP缓存中IP地址和MAC地址的对应关系、添加或删除静态对应关系等。**

**逆地址解析协议，即RARP，功能和ARP协议相对，其将局域网中某个主机的物理地址转换为IP地址**

，比如局域网中有一台主机只知道物理地址而不知道IP地址，那么可以通过RARP协议发出征求自身IP地址的广播请求，然后由RARP服务器负责回答。

ARP工作流程举例：

主机A的IP地址为192.168.1.1，MAC地址为0A-11-22-33-44-01；

主机B的IP地址为192.168.1.2，MAC地址为0A-11-22-33-44-02；

当主机A要与主机B通信时，地址解析协议可以将主机B的IP地址（192.168.1.2）解析成主机B的MAC地址



## 举例

在浏览器中输入 **http://www.baidu.com/** 后执行的全部过程。

现在假设如果我们在浏览器（客户端）中输入 http://www.baidu.com， 而 baidu.com 为要访问的服务器（服务器），下面详细分析客户端为了访问服务器而执行的一系列关于协议的操作：

- 1）客户端浏览器通过DNS解析到www.baidu.com的IP地址220.181.27.48，通过这个IP地址找到客户端到服务器的路径。客户端浏览器发起一个HTTP会话到220.161.27.48，然后通过TCP进行封装数据包，输入到网络层。
- 2）在客户端的传输层，把HTTP会话请求分成报文段，添加源和目的端口，如服务器使用80端口监听客户端的请求，客户端由系统随机选择一个端口如5000，与服务器进行交换，服务器把相应的请求返回给客户端的5000端口。然后使用IP层的IP地址查找目的端。
- 3）客户端的网络层不用关心应用层或者传输层的东西，主要做的是通过查找路由表确定如何到达服务器，期间可能经过多个路由器，这些都是由路由器来完成的工作，不作过多的描述，无非就是通过查找路由表决定通过那个路径到达服务器。
- 4）客户端的链路层，包通过链路层发送到路由器，通过邻居协议查找给定IP地址的MAC地址，然后发送ARP请求查找目的地址，如果得到回应后就可以使用ARP的请求应答交换的IP数据包现在就可以传输了，然后发送IP数据包到达服务器的地址。

### 从输入网址到获得页面的过程

　　(1). 浏览器（客户端）查询 DNS，获取域名对应的IP地址。

　　(2). 浏览器（客户端）获得域名对应的IP地址以后，浏览器（客户端）向服务器请求建立链接，发起三次握手；

　　(3). TCP/IP链接建立起来后，浏览器（客户端）向服务器发送HTTP请求；

　　(4). 服务器接收到这个请求，并根据路径参数映射到特定的请求处理器进行处理，并将处理结果及相应的视图返回给浏览器（客户端）；

　　(5). 浏览器（客户端）解析并渲染视图，若遇到对js文件、css文件及图片等静态资源的引用，则重复上述步骤并向服务器请求这些资源；

　　(6). 浏览器（客户端）根据其请求到的资源、数据渲染页面，最终向用户呈现一个完整的页面。



## 集线器、交换机、路由器的工作原理及其区别

> https://www.bilibili.com/video/BV1LC4y187Ew/?spm_id_from=autoNext

**集线器**（HUB）属于数据通信系统中的基础设备，它和双绞线等传输介质一样，是一种不需任何软件支持或只需很少管理软件管理的硬件设备。

**交换机**(Switch)是一种基于MAC（网卡的硬件地址）识别，能完成封装转发数据包功能的网络设备。

**路由器**(Router)亦称选径器，是一种连接多个网络或网段的网络设备，它能将不同网络或网段之间的数据信息进行“翻译”，以使它们能够相互“读”懂对方的数据，从而构成一个更大的网络是在网络层实现互连的设备。

从OSI体系结构来看，**集线器属于OSI的第一层物理层设备**，**交换机属于OSI的第二层数据链路层设备**。**路由器一开始就设计工作在OSI模型的网络层（第三层）**，可以得到更多的协议信息，路由器可以做出更加智能的转发决策。 

路由器提供了防火墙的服务 。



## 常见状态码及原因短语

HTTP请求结构： 请求方式 + 请求URI + 协议及其版本
HTTP响应结构： 状态码 + 原因短语 + 协议及其版本

1×× : 请求处理中，请求已被接受，正在处理
2×× : 请求成功，请求被成功处理
200 OK
3×× : 重定向，要完成请求必须进行进一步处理
301 : 永久性转移
302 ：暂时性转移
304 ： 已缓存
4×× : 客户端错误，请求不合法
400：Bad Request,请求有语法问题
403：拒绝请求
404：客户端所访问的页面不存在
5×× : 服务器端错误，服务器不能处理合法请求
500 ：服务器内部错误
503 ： 服务不可用，稍等



# Operating System

```text
CPU内核、GPU/CPU优缺点、固态硬盘/机械硬盘优缺点、中断、微内核、进程间通信、虚拟内存、内核态/用户态
```

> https://zhuanlan.zhihu.com/p/23755202?refer=passer
>
> https://www.douban.com/note/695591186/

## 多线程的优点，为什么需要线程？

- 优点：
    1.使用线程可以把占据时间长的程序中的任务放到后台去处理
    2.用户界面更加吸引人,这样比如用户点击了一个按钮去触发某件事件的处理,可以弹出一个进度条来显示处理的进度
    3.程序的运行效率可能会提高
    4.在一些等待的任务实现上如用户输入,文件读取和网络收发数据等,线程就比较有用了.
- 缺点：
  1.如果有大量的线程,会影响性能,因为操作系统需要在它们之间切换.
    2.更多的线程需要更多的内存空间
    3.线程中止需要考虑对程序运行的影响.
    4.通常块模型数据是在多个线程间共享的,需要防止线程死锁情况的发生

## 内存对齐？

```C
struct A
{
	char a;
    short b;
   	int c;
};
struct B
{
    short b;
    int c;
   	char a;
};
int main()
{
 	cout<<sizeof(A)<<endl;//8 = 1+(1)+2+4
 	cout<<sizeof(B)<<endl;//12 = 2+(2)+4+1+(3)
 	return 0;
 }
```



- 为什么要进行内存对齐？
  **一、硬件原因：加快CPU访问的速度**
  我们大多数人在没有搞清楚CPU是如何读取数据的时候，基本都会认为CPU是一字节一字节读取的，但实际上**CPU是按照块来读取的，块的大小可以为2，4，8，16。块的大小也称为内存读取粒度。**
  假设CPU没有内存对齐，要读取一个4字节的数据到一个寄存器中，（假设读取粒度为4），则会出现两种情况
  1、数据的开始在CPU读取的0字节处，这刚CPU一次就你能够读取完毕
  2、数据的开始没在0字节处，假设在1字节处吧，CPU要先将0 ~ 3字节读取出来，在读取4~ 7字节的内容。然后将0~ 3字节里的0字节丢弃，将4~7字节里的5，6，7字节的数据丢弃。然后组合1,2,3,4的数据。
  由此可以看出，CPU读取的效率不是很高，可以说比较繁琐。
  但如果有内存对齐的话：
  由于每一个数据都是对齐好的，CPU可以一次就能够将数据读取完成，虽然会有一些内存碎片，但从整个内存的大小来说，都不算什么，可以说是用空间换取时间的做法。
  **二、平台原因：**
  不是所有的硬件平台都可以访问任意地址上的任意数据，**某些硬件平台只能在某些地址处取某些类型的数据**，否则抛出硬件异常。



## 进程、线程、协程、IO多路复用、并发？

- **协程**：跟用线程替代进程处理服务是同个性质，不过协程是线程中划分出的更小单位、由程序本身控制，没有线程切换的开销（降低了资源消耗）

- [I/O多路复用](https://mp.weixin.qq.com/s?__biz=MzI4Njg5MDA5NA==&mid=2247485351&idx=1&sn=81cdc2220a1a6f402fbbcf45b867365f&chksm=ebd746a6dca0cfb0d62d23fb7cfeb446caed776f6792d99d9f3c1aee8ef018caa70dbf04bbbf&mpshare=1&scene=1&srcid=&key=4aaedc7deb86b11c8984ec8a98934e073f9d18a31761fa9cf07b3b7addb20a88e1e9579f68755833e2bba6a81550ef4121090a8a22729cbc7c0377001c67465beca7351fb183dc9000c83f501dd979a0&ascene=1&uin=MjQyNjk1NDk0NA%3D%3D&devicetype=Windows+10&version=62060833&lang=zh_CN&pass_ticket=j%2FoH3wB2Xy5uVqeYxWzp8UhEFo3b6I%2BS%2BQ6nuTAfeNWaspraBSMXRYyfrcCByFAc)：让一个CPU执行单元同时服务n个请求，原本是一个进程跟进一个socket进行服务，如果有20个socket，就需要20个进程，而I/O多路复用建立不同功能的线程（轮询、接受数据、处理数据等等），这样其实用6个线程就可以解决了。
  **这种处理模式就是多路复用I/O，多路指的是多个Socket通道，复用指的是只用一个线程来管理它们。**

- **并发**：要做到同时服务多个客户端，有三种技术：

  1.**进程并行**：只能开到当前cpu个数的进程，但能用来处理计算型任务 ，开销最大

  2.如果并行不必要，那么可以考虑用**线程并发**，单位开销比进程小很多
  	**阻塞式IO**：一个线程负责一个socket，检测是否有数据处理，没有则阻塞。
  	**非阻塞式IO**：每个线程占用不释放CPU，循环检测是否有数据处理，这种称为**忙等待**，会空耗CPU。

  3.如果轮询不必要，可考虑是否可以只需要遇到阻塞切换
  	就可以用**IO多路复用技术 + 协程**来实现阻塞切换，消耗资源很少，并发量最高

## 进程间通信、同步、异步方法？

### 进程通信

低级通信 - 控制信息的通信：**信号、信号量**。
高级通信 - 数据信息的通信：**管道、信息队列、共享内存、套接字**。其中只有套接字可用于不同主机的进程通信。

**信号**：信号**主要用于通知某个进程发生了什么事**，就像你打电话通知某个人某件事一样，事先注册号信号相应的注册函数就可以了。

**信号量**：信号量实际上是一个计数器，通常在多线程或者多进程开发中会用到，**主要用来控制多线程多进程对于共享资源访问**，通常配合锁来实现同时只有一个进程或者线程操作共享资源，防止数据的不同步。

**管道**：管道通信方式的**中间介质是文件**，通常称这种文件为管道文件。管道的实质是一个内核缓冲区，进程以先进先出的方式从缓冲区存取数据：管道一端的进程顺序地将进程数据写入缓冲区，另一端的进程则顺序地读取数据，该缓冲区可以看做一个循环队列，读和写的位置都是自动增加的，一个数据只能被读一次，读出以后再缓冲区都不复存在了。当缓冲区读空或者写满时，有一定的规则控制相应的读进程或写进程是否进入等待队列，当空的缓冲区有新数据写入或慢的缓冲区有数据读出时，就唤醒等待队列中的进程继续读写。
PS：管道是远古进程间通信方式，感觉没啥优点。

![在这里插入图片描述](MyNotes.assets/20190426140549553.png)

> **无名管道** ：主要用于父进程与子进程之间，或者两个兄弟进程之间。
>
> **有名（命名）管道**：建立在实际的磁盘介质或文件系统（而不是只存在于内存中）上有自己名字的文件，任何进程可以在任何时间通过文件名或路径名与该文件建立联系。

**消息队列**：消息队列是由消息的链表，存放在内核中并由消息队列标识符标识。消息队列克服了信号传递信息少、**管道只能承载无格式字节流以及缓冲区大小受限等缺点**。对每个消息指定特定的消息类型，接收的时候不需要按照队列次序，而是**可以根据自定义条件接收特定类型的消息**。

**共享内存**：这个是经常用的，共享内存号称是最快的进程间通信方式，她**在系统内存中开辟一块内存区，分别映射到各个进程的虚拟地址空间中**，任何一个进程操作了内存区都会反映到其他进程中，各个进程之间的通信并没有像copy数据一样从内核到用户，再从用户到内核的拷贝。

```
共享内存区是最快的可用IPC形式，一旦这样的内存区映射到共享它的进程的地址空间，这些进程间数据的传递就不再通过执行任何进入内核的系统调用来传递彼此的数据，节省了时间。

共享内存和消息队列，FIFO，管道传递消息的区别：
——消息队列，FIFO，管道的消息传递方式一般为
1：服务器得到输入
2：通过管道，消息队列写入数据，通常需要从进程拷贝到内核。
3：客户从内核拷贝到进程
4：然后再从进程中拷贝到输出文件
上述过程通常要经过4次拷贝，才能完成文件的传递。

——共享内存只需要
1:从输入文件到共享内存区
2:从共享内存区输出到文件
上述过程不涉及到内核的拷贝，所以花的时间较少。

队列通信中的消息有明显的生命周期，消息有传递的过程，有通知的过程，消息有失效性，有先后关系。
而共享内存没有上面这些特点。共享内存在使用时要解决互斥的问题。
```

**socket**：socket是一种面向网络的一种进程间通信方式，只要有网络存在，它可以跨越任何限制。socket可以实现不同主机进程间的通信。

### 进程同步

1、**临界区（Critical Section）**:通过对多线程的串行化来访问公共资源或一段代码，速度快，适合控制数据访问。

优点：保证在某一时刻只有一个线程能访问数据的简便办法

缺点：虽然临界区同步速度很快，但却**只能用来同步本进程内的线程**，而不可用来同步多个进程中的线程。 

2、**互斥量（Mutex）**:为协调共同对一个共享资源的单独访问而设计的。

互斥量跟临界区很相似，比临界区复杂，互斥对象只有一个，只有拥有互斥对象的线程才具有访问资源的权限。

优点：使用互斥不仅仅能够在同一应用程序不同线程中实现资源的安全共享，而且可以在不同应用程序的线程之间实现对资源的安全共享。

缺点：①互斥量是可以命名的，也就是说它可以跨越进程使用，所以**创建互斥量需要的资源更多**，所以如果只为了在进程内部是用的话使用临界区会带来速度上的优势并能够减少资源占用量。因为互斥量是跨进程的互斥量一旦被创建，就可以通过名字打开它。

②通过互斥量可以指定资源被独占的方式使用，但如果有下面一种情况通过互斥量就无法处理，比如现在一位用户购买了一份三个并发访问许可的数据库系统，可以根据用户购买的访问许可数量来决定有多少个线程/进程能同时进行数据库操作，这时候如果利用互斥量就没有办法完成这个要求，信号量对象可以说是一种资源计数器。 

3、**信号量（Semaphore）**:为控制一个具有有限数量用户资源而设计。它允许多个线程在同一时刻访问同一资源，但是需要限制在同一时刻访问此资源的最大线程数目。互斥量是信号量的一种特殊情况，当信号量的最大资源数=1就是互斥量了。

优点：**适用于对Socket（套接字）程序中线程的同步**。（例如，网络上的HTTP服务器要对同一时间内访问同一页面的用户数加以限制，只有不大于设定的最大用户数目的线程能够进行访问，而其他的访问企图则被挂起，只有在有用户退出对此页面的访问后才有可能进入。）

缺点：①信号量机制**必须有公共内存**，不能用于分布式操作系统，这是它最大的弱点；

②信号量机制功能强大，但使用时对信号量的操作分散， 而且难以控制，读写和维护都很困难，加重了程序员的编码负担；

③核心操作**P-V分散在各用户程序的代码中，不易控制和管理**，一旦错误，后果严重，且不易发现和纠正。 

4、**事件（Event）**: 用来通知线程有一些事件已发生，从而启动后继任务的开始。

优点：事件对象通过通知操作的方式来保持线程的同步，并且可以实现不同进程中的线程同步操作。

缺点：... 

**总结：**

①**临界区不是内核对象，只能用于进程内部的线程同步**，是用户方式的同步。互斥、信号量是内核对象可以用于不同进程之间的线程同步（跨进程同步）。
②互斥其实是信号量的一种特殊形式。**互斥可以保证在某一时刻只有一个线程可以拥有临界资源**。**信号量可以保证在某一时刻有指定数目的线程可以拥有临界资源**。



### 进程异步

**进程异步**：异步的概念和同步相对。当一个异步过程调用发出后，调用者不能立刻得到结果。实际处理这个调用的部件在完成后，**通过状态、通知和回调来通知调用者**。

举例：两个进程访问一个临界资源，如果进程A正在访问临界资源，这时B进程也访问临界资源，发现被占用，于是B就会去干自己的事情，等到A进程释放临界资源，会通知B，B就会来访问资源。

进程同步/异步指的是进程之间的**运行关系**，但是阻塞和非阻塞是访问资源的一种**运行状态**

## TCP、HTTP、Socket的区别？

> https://www.cnblogs.com/xuan52rock/p/9454696.html

TCP/IP只是一个协议栈，就像操作系统的运行机制一样，必须要具体实现，同时还要提供对外的操作接口。

HTTP协议是建立在TCP协议之上的一种应用。

套接字（socket）是通信的基石，是支持TCP/IP协议的网络通信的基本操作单元。它是网络通信过程中端点的抽象表示，包含进行网络通信必须的五种信息：连接使用的协议，本地主机的IP地址，本地进程的协议端口，远地主机的IP地址，远地进程的协议端口。

建立网络通信连接至少要一对端口号（socket）。**socket本质是编程接口（API），对TCP/IP的封装，TCP/IP也要提供可供程序员做网络开发所用的接口，这就是Socket编程接口；HTTP是轿车，提供了封装或者显示数据的具体形式；Socket是发动机，提供了网络通信的能力。**



## 公钥加密、私钥加密、对称/非对称加密？

**对称加密**采用了对称密码编码技术，它的特点是文件加密和解密使用**相同的密钥**加密，密钥不能公开。

**非对称加密**中需要两个密钥：公开密钥（publickey）和私有密钥（privatekey）。这有两个作用：

- 一是用来**加解密**：公钥加密，私钥解密。 （`公钥加密`）
- 二是用来**签名**：私钥签名，公钥验证。 （`私钥加密`）



## 进程有哪几种状态？

- 就绪状态：进程已获得除处理机以外的所需资源，等待分配处理机资源
- 运行状态：占用处理机资源运行，处于此状态的进程数小于等于CPU数
- 阻塞状态： 进程等待某种条件，在条件满足之前无法执行

![img](MyNotes.assets/image.mamicode.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg)

## 缓冲区溢出？

缓冲区溢出是指当计算机向缓冲区填充数据时超出了缓冲区本身的容量，溢出的数据覆盖在合法数据上。

危害有以下两点：

- 程序崩溃，导致拒绝额服务
- 跳转并且执行一段恶意代码

造成缓冲区溢出的主要原因是程序中没有仔细检查用户输入。



## 进程和线程以及它们的区别

**进程是对运行时程序的封装，是系统进行资源调度和分配的的基本单位,例如打开QQ或者微信都是一个进程。**，实现了操作系统的并发；进程是占有资源的最小单位，这个资源当然包括内存。在操作系统的角度来看，进程=程序+数据+PCB(进程控制块)。

**线程是进程的子任务，是CPU调度和分派的基本单位**，用于保证程序的实时性，实现进程内部的并发；一个程序至少有一个进程，一个进程至少有一个线程，线程依赖于进程而存在；

进程在执行过程中拥有独立的内存单元，而多个线程共享进程的内存。



## 什么是死锁？死锁产生的条件？

1). 死锁的概念

　　在两个或者多个并发进程中，如果每个进程持有某种资源而又等待其它进程释放它或它们现在保持着的资源，在未改变这种状态之前都不能向前推进，称这一组进程产生了死锁。通俗的讲，就是两个或多个进程无限期的阻塞、相互等待的一种状态。

2). 死锁产生的四个必要条件

**互斥:** 进程对所分配的资源进行排他性的使用
**占有并等待：** 进程被阻塞的时候并不释放锁申请到的资源
**不可抢占：** 进程对于已经申请到的资源在使用完成之前不可以被剥夺
**循环等待：** 发生死锁的时候存在的一个 进程-资源环形等待链

**3). 处理死锁的解决方法**
预防死锁： 破坏产生死锁的4个必要条件中的一个或者多个；实现起来比较简单，但是如果限制过于严格会降低系统资源利用率以及吞吐量

避免死锁： 在资源的动态分配中，防止系统进入不安全状态(可能产生死锁的状态)、比如经典的**银行家算法**。

检测死锁： 允许系统运行过程中产生死锁，在死锁发生之后，采用一定的算法进行检测，并确定与死锁相关的资源和进程，采取相关方法清除检测到的死锁。实现难度大

解除死锁： 与死锁检测配合，将系统从死锁中解脱出来（撤销进程或者剥夺资源）。对检测到的和死锁相关的进程以及资源，通过撤销或者挂起的方式，释放一些资源并将其分配给处于阻塞状态的进程，使其转变为就绪态。实现难度大



## 银行家算法

> https://www.bilibili.com/video/BV1rJ411p7au/?spm_id_from=333.788.recommend_more_video.3
>
> https://blog.csdn.net/flowing_wind/article/details/82156968

**银行家算法是最具代表性的避免死锁的方法**。每一个新进程进入系统时，必须声明需要每种资源的最大数目，其数目不能超过系统所拥有的的资源总量。当进程请求一组资源时，系统必须首先确定是否有足够的资源分配给该进程，若有，再进一步计算在将这些资源分配给进程后，是否会使系统处于不安全状态如果不会才将资源分配给它，否则让进程等待。

银行家算法中的数据结构：
（1）Available向量：系统中可利用的资源数目
（2）Max矩阵：每个进程对每种资源的最大需求
（3）Allocation矩阵：每个进程已分配的各类资源的数目
（4）Need矩阵：每个进程尚需的各类资源数

假定系统中有五个进程{p0,p1,p2,p3,p4}和三类资源{A,B,C}各类资源的数目分别是10,5,7，已知T0时刻资源分配情况如下，判断系统是否安全：

![这里写图片描述](MyNotes.assets/20180828235558795)

流程：

1. 先画表1{Max，Allocation，Need，Available}；
2. 表1中只要能找到满足Need小于Available，就分配资源，让该进程运行完成，最后将资源释放，更新表2{Work，Allocation，Need，Work+Allocation}；
3. 如果最后每一个进程都能运行完成就得到了我们的安全序列，所以系统就是安全的

假设P1请求资源Request{1,0,2},要怎样处理呢?

![这里写图片描述](MyNotes.assets/20180829003840213)

流程：

1. 判断Request≤Need；
2. 判断Request≤Available；
3. 将资源分配给该进程，更新Allocation，Need和Available；
4. 进行安全状态判断；



## 分页和分段有什么区别（内存管理有几种方式）？

内存管理有块式管理，页式管理，段式和段页式管理。现在常用段页式管理。

- 块式管理：把主存分为一大块、一大块的，当所需的程序片断不在主存时就分配一块主存空间，把程序片断load入主存，就算所需的程序片度只有几个字节也只能把这一块分配给它。这样会造成很大的浪费，平均浪费了50％的内存空间，但是易于管理。
- 页式管理：把主存分为一页一页的，每一页的空间要比一块一块的空间小很多，显然这种方法的空间利用率要比块式管理高很多。
- 段式管理：把主存分为一段一段的，每一段的空间又要比一页一页的空间小很多，这种方法在空间利用率上又比页式管理高很多，但是也有另外一个缺点。一个程序片断可能会被分为几十段，这样很多时间就会被浪费在计算每一段的物理地址上。
- 段页式管理：结合了段式管理和页式管理的优点。将程序分成若干段，每个段分成若干页。段页式管理每取一数据，要访问3次内存。

**段式存储管理**是一种符合用户视角的内存分配管理方案。在段式存储管理中，**将程序的地址空间划分为若干段（segment），如代码段，数据段，堆栈段；**这样每个进程有一个二维地址空间，相互独立，互不干扰。段式管理的优点是：**没有内碎片**（因为段大小可变，改变段大小来消除内碎片）。但**段换入换出时，会产生外碎片**（比如4k的段换5k的段，会产生1k的外碎片）

**页式存储管理**方案是一种用户视角内存与物理内存相分离的内存分配管理方案。在页式存储管理中，**将程序的逻辑地址划分为固定大小的页**（page），而物理内存划分为同样大小的帧，程序加载时，可以将任意一页放入内存中任意一个帧，这些**帧不必连续**，从而实现了离散分离。页式存储管理的优点是：**没有外碎片（因为页的大小固定），但会产生内碎片（一个页可能填充不满）**。

两者的不同点：

**目的不同**：分页是由于系统管理的需要而不是用户的需要，它是信息的物理单位；分段的目的是为了能更好地满足用户的需要，它是信息的逻辑单位，它含有一组其意义相对完整的信息；

**大小不同**：页的大小固定且由系统决定，而段的长度却不固定，由其所完成的功能决定；

**地址空间不同**： 段向用户提供二维地址空间；页向用户提供的是一维地址空间；

**信息共享**：段是信息的逻辑单位，便于存储保护和信息的共享，页的保护和共享受到限制；

**内存碎片**：页式存储管理的优点是没有外碎片（因为页的大小固定），但会产生内碎片（一个页可能填充不满）；而段式管理的优点是没有内碎片（因为段大小可变，改变段大小来消除内碎片）。但段换入换出时，会产生外碎片（比如4k的段换5k的段，会产生1k的外碎片）。



## 操作系统中进程调度策略有哪几种？

**FIFO或First Come, First Served (FCFS)先来先服务**

- 调度的顺序就是任务到达就绪队列的顺序
- 公平、简单(FIFO队列)、非抢占、不适合交互式
- 未考虑任务特性，平均等待时间可以缩短

**Shortest Job First (SJF)**

- 最短的作业(CPU区间长度最小)最先调度
- SJF可以保证最小的平均等待时间

**Shortest Remaining Job First (SRJF)**

- SJF的可抢占版本，比SJF更有优势
- SJF(SRJF): 如何知道下一CPU区间大小？根据历史进行预测: 指数平均法

**优先权调度**

- 每个任务关联一个优先权，调度优先权最高的任务
- 注意：优先权太低的任务一直就绪，得不到运行，出现“饥饿”现象



## 什么是虚拟内存？

虚拟内存是计算机系统内存管理的一种技术。它使得应用程序认为它拥有连续的可用的内存（一个连续完整的地址空间），而实际上，**它通常是被分隔成多个物理内存碎片，还有部分暂时存储在外部磁盘存储器上，在需要时进行数据交换**。目前，大多数操作系统都使用了虚拟内存，如Windows家族的“虚拟内存”；Linux的“交换空间”等。

![image-20210423120136142](MyNotes.assets/image-20210423120136142.png)

![这里写图片描述](MyNotes.assets/20171021161259614)

>  由图可看出，虚拟内存实际上可以比物理内存大。当访问虚拟内存时，会访问MMU（内存管理单元）去匹配对应的物理地址（比如图5的0，1，2）。如果虚拟内存的页并不存在于物理内存中（如图5的3,4），会产生缺页中断，从磁盘中取得缺的页放入内存，如果内存已满，还会根据某种算法将磁盘中的页换出。



### 虚拟内存的应用与优点

虚拟内存很适合在多道程序设计系统中使用，许多程序的片段同时保存在内存中。当一个程序等待它的一部分读入内存时，可以把CPU交给另一个进程使用。虚拟内存的使用可以带来以下好处：

- **在内存中可以保留多个进程，系统并发度提高**
- 解除了用户与内存之间的紧密约束，**进程可以比内存的全部空间还大**



## 页面置换算法

**FIFO(First In, First Out) 先进先出算法**：在操作系统中经常被用到，比如作业调度（主要实现简单，很容易想到）；

**LRU（Least recently use）最近最少使用算法**：根据使用**时间**到现在的长短来判断，淘汰最近最长未使用的页；

**LFU（Least frequently use）最少使用次数算法**：根据使用**次数**来判断；

**OPT（Optimal replacement）最优置换算法**：理论的最优，这听起来很简单，但是无法实现。就是要**保证置换出去的是不再被使用的页**，或者是在实际内存中最晚使用的算法。



## 空间不足的解决方案

当预留的空间不够满足增长时，操作系统首先会看相邻的内存是否空闲，如果空闲则自动分配，如果不空闲，就将整个进程移到足够容纳增长的空间内存中，如果不存在这样的内存空间，则会将闲置的进程置换出去。



## 字节序

计算机硬件有两种储存数据的方式：大端字节序（big endian）和小端字节序（little endian）。

举例来说，数值`0x2211`使用两个字节储存：高位字节是`0x22`，低位字节是`0x11`。

- **大端字节序**：高位字节在前，低位字节在后，这是人类读写数值的方法。
- **小端字节序**：低位字节在前，高位字节在后，即以`0x1122`形式储存。

为什么会有小端字节序？

答案是，计算机电路先处理低位字节，效率比较高，因为**计算都是从低位开始的。所以，计算机的内部处理都是小端字节序。**
但是，人类还是习惯读写大端字节序。所以，除了计算机的内部处理，其他的场合几乎都是大端字节序，比如网络传输和文件储存。

> **"只有读取的时候，才必须区分字节序，其他情况都不用考虑。"**



## 零拷贝

考虑这样一种常用的情形 ： 你需要将静态内容(类似图片、文件)展示给用户 。 这个情形就意味着需要先将静态内容从磁盘中复制出来放到一个内存 buf 中，然后将这个 buf 通过套接字（Socket）传输给用户，进而用户获得静态内容 。

![img](MyNotes.assets/v2-7fe2d34533a2762e4a2a3f0bdd5f7209_720w.jpg)

如果采用了零拷贝技术，那么应用程序可以直接请求内核把磁盘中的数据传输给 Socket, 如下图所示。

![img](MyNotes.assets/v2-19e824356374ffa2e1cd1c9e6befc078_720w.jpg)

改进的地方：我们将上下文切换的次数从四次减少到了两次，将数据复制的次数从四次减少到了三次（其中只有一次涉及到了 CPU）。

但是这个代码尚未达到我们的零拷贝要求。如果底层网络接口卡支持*收集操作* 的话，那么我们就可以进一步减少内核的数据复制。在 Linux 内核 2.4 及后期版本中，套接字缓冲区描述符就做了相应调整，以满足该需求。这种方法不仅可以减少多个上下文切换，还可以消除需要涉及 CPU 的重复的数据拷贝。

![img](MyNotes.assets/v2-61882b6e8a11b6ae6d60f69ba12fdbd9_720w.jpg)

# Dataset

## drop、delete**与**truncate分别在什么场景之下使用？

- 不再需要一张表的时候，用**drop**
- 想删除部分数据行时候，用**delete**，并且带上where子句
- 保留表而删除所有数据的时候用**truncate**

## 超键、候选键、主键、外键分别是什么？

### 定义：

**超键(super key)**: 在关系中能唯一标识元组的属性集称为关系模式的超键

**候选键(candidate key)**: 不含有多余属性的超键称为候选键。也就是在候选键中，若再删除属性，就不是键了！

**主键(primary key)**: 用户选作元组标识的一个候选键程序主键

**外键(foreign key)**：如果关系模式R中属性K是其它模式的主键，那么k在模式R中称为外键。

### 实例：
学生信息（学号 身份证号 性别 年龄 身高 体重 宿舍号）和 宿舍信息（宿舍号 楼号）

超键：只要含有“学号”或者“身份证号”两个属性的集合就叫超键，例如R1（学号 性别）、R2（身份证号 身高）、R3（学号 身份证号）等等都可以称为超键！

候选键：不含有多余的属性的超键，比如（学号）、（身份证号）都是候选键，又比如R1中学号这一个属性就可以唯一标识元组了，而有没有性别这一属性对是否唯一标识元组没有任何的影响！

主键：就是用户从很多候选键选出来的一个键就是主键，比如你要求学号是主键，那么身份证号就不可以是主键了！

外键：宿舍号就是学生信息表的外键

## 什么是视图？以及视图的使用场景有哪些？

**视图是一种虚拟的表**，具有和物理表相同的功能。可以对视图进行增，改，查，操作，试图通常是有一个表或者多个表的行或列的子集。对视图的修改不影响基本表。它使得我们获取数据更容易，相比多表查询。

- **只暴露部分字段给访问者**，所以就建一个虚表，就是视图。
- 查询的数据来源于不同的表，而查询者希望以统一的方式查询，这样也可以建立一个视图，把多个表查询结果联合起来，查询者只需要直接从视图中获取数据，不必考虑数据来源于不同表所带来的差异

## 说一说三个范式

- **第一范式（1NF）：强调的是列的原子性，即列不能够再分成其他几列。** 
  考虑这样一个表：【联系人】（姓名，性别，电话） 
  如果在实际场景中，一个联系人有家庭电话和公司电话，那么这种表结构设计就没有达到 1NF。要符合 1NF 我们只需把列（电话）拆分，即：【联系人】（姓名，性别，家庭电话，公司电话）。
-  **第二范式（2NF）：首先是 1NF，另外包含两部分内容，一是表必须有一个主键；二是没有包含在主键中的列必须完全依赖于主键，而不能只依赖于主键的一部分。** （**完全依赖**）
  考虑一个订单明细表：【OrderDetail】（OrderID，ProductID，UnitPrice，Discount，Quantity，ProductName）。
  因为我们知道在一个订单中可以订购多种产品，所以单单一个 OrderID 是不足以成为主键的，主键应该是（OrderID，ProductID）。显而易见 Discount（折扣），Quantity（数量）完全依赖（取决）于主键（OderID，ProductID），而 UnitPrice，ProductName 只依赖于 ProductID。所以 OrderDetail 表不符合 2NF。不符合 2NF 的设计容易产生冗余数据。
  可以把【OrderDetail】表拆分为【OrderDetail】（OrderID，ProductID，Discount，Quantity）和【Product】（ProductID，UnitPrice，ProductName）来消除原订单表中UnitPrice，ProductName多次重复的情况。
- **第三范式（3NF）：首先是 2NF，另外非主键列必须直接依赖于主键，不能存在传递依赖。**即不能存在：非主键列 A 依赖于非主键列 B，非主键列 B 依赖于主键的情况。（**不存在传递依赖**）
  考虑一个订单表【Order】（OrderID，OrderDate，CustomerID，CustomerName，CustomerAddr，CustomerCity）主键是（OrderID）。
  其中 OrderDate，CustomerID，CustomerName，CustomerAddr，CustomerCity 等非主键列都完全依赖于主键（OrderID），所以符合 2NF。不过问题是 CustomerName，CustomerAddr，CustomerCity 直接依赖的是 CustomerID（非主键列），而不是直接依赖于主键，它是通过传递才依赖于主键，所以不符合 3NF。
  通过拆分【Order】为【Order】（OrderID，OrderDate，CustomerID）和【Customer】（CustomerID，CustomerName，CustomerAddr，CustomerCity）从而达到 3NF。

>  第二范式（2NF）和第三范式（3NF）的概念很容易混淆，区分它们的关键点在于，2NF：非主键列是否完全依赖于主键，还是依赖于主键的一部分；3NF：非主键列是直接依赖于主键，还是直接依赖于非主键列。、



# Algorithms

1. 排序算法：快速排序、归并排序、计数排序
2. 搜索算法：回溯、递归、剪枝技巧
3. 图论：最短路、最小生成树、网络流建模
4. 动态规划：背包问题、最长子序列、计数问题
5. 基础技巧：分治、倍增、二分、贪心



## 最短路径算法

> https://zhuanlan.zhihu.com/p/54510444

### 广度优先搜索

从起点开始，首先遍历起点周围邻近的点，然后再遍历已经遍历过的点邻近的点，逐步的向外扩散，直到找到终点。

### Dijkstra算法

在Dijkstra算法中，需要计算每一个节点距离起点的总移动代价。同时，还需要一个优先队列结构。对于所有待遍历的节点，放入优先队列中会按照代价进行排序。

在算法运行的过程中，每次都从优先队列中选出代价最小的作为下一个遍历的节点。直到到达终点为止。

### A*算法

A*算法通过下面这个函数来计算每个节点的优先级。

$$f(n)=g(n)+h(n)$$

其中：

- $f(n)$是节点n的综合优先级。当我们选择下一个要遍历的节点时，我们总会选取综合优先级最高（值最小）的节点。
- $g(n)$ 是节点n距离起点的代价。
- $h(n)$是节点n距离终点的预计代价，这也就是A*算法的启发函数。

A*算法在运算过程中，每次从优先队列中选取f(n)值最小（优先级最高）的节点作为下一个待遍历的节点。

另外，A*算法使用两个集合来表示待遍历的节点，与已经遍历过的节点，这通常称之为`open_set`和`close_set`。

```text
* 初始化open_set和close_set；
* 将起点加入open_set中，并设置优先级为0（优先级最高）；
* 如果open_set不为空，则从open_set中选取优先级最高的节点n：
    * 如果节点n为终点，则：
        * 从终点开始逐步追踪parent节点，一直达到起点；
        * 返回找到的结果路径，算法结束；
    * 如果节点n不是终点，则：
        * 将节点n从open_set中删除，并加入close_set中；
        * 遍历节点n所有的邻近节点：
            * 如果邻近节点m在close_set中，则：
                * 跳过，选取下一个邻近节点
            * 如果邻近节点m也不在open_set中，则：
                * 设置节点m的parent为节点n
                * 计算节点m的优先级
                * 将节点m加入open_set中       
```

> 在极端情况下，当启发函数h(n)始终为0，则将由g(n)决定节点的优先级，此时算法就退化成了Dijkstra算法。
>
> 如果h(n)始终小于等于节点n到终点的代价，则A*算法保证一定能够找到最短路径。但是当h(n)的值越小，算法将遍历越多的节点，也就导致算法越慢。
>
> 如果h(n)完全等于节点n到终点的代价，则A*算法将找到最佳路径，并且速度很快。可惜的是，并非所有场景下都能做到这一点。因为在没有达到终点之前，我们很难确切算出距离终点还有多远。
>
> 如果h(n)的值比节点n到终点的代价要大，则A*算法不能保证找到最短路径，不过此时会很快。
>
> 在另外一个极端情况下，如果h(n)相较于g(n)大很多，则此时只有h(n)产生效果，这也就变成了最佳优先搜索。
>
> 由上面这些信息我们可以知道，通过调节启发函数我们可以控制算法的速度和精确度。因为在一些情况，我们可能未必需要最短路径，而是希望能够尽快找到一个路径即可。这也是A*算法比较灵活的地方。
>
> 对于网格形式的图，有以下这些**启发函数**可以使用（**计算h(n)是，要忽略路径中的障碍物。这是对剩余距离的估算值，而不是实际值**）：
>
> - 如果图形中只允许朝上下左右四个方向移动，则可以使用曼哈顿距离（Manhattan distance）。
> - 如果图形中允许朝八个方向移动，则可以使用对角距离。
> - 如果图形中允许朝任何方向移动，则可以使用欧几里得距离（Euclidean distance）。

### D*算法

> A* 在静态路网中非常有效（very efficient for static worlds），但不适于在动态路网，环境如权重等不断变化的动态环境下。 
>
> D*是动态A *（D-Star,Dynamic A Star）算法

相比A-star算法，**D-star的主要特点就是由目标位置开始向起始位置进行路径搜索**，当物体由起始位置向目标位置运行过程中，发现路径中存在新的障碍时，对于目标位置到新障碍之间的范围内的路径节点，新的障碍是不会影响到其到目标的路径的。新障碍只会影响的是物体所在位置到障碍之间范围的节点的路径。在这时通过 将新的障碍周围的节点加入到Openlist中进行处理然后向物体所在位置进行传播，能最小程度的减少计算开销。



# Data Structures

1. 数组与链表：单 / 双向链表、跳舞链
2. 栈与队列
3. 树与图：最近公共祖先、并查集、红黑树
4. 哈希表
5. 堆：大 / 小根堆、可并堆
6. 字符串：字典树、后缀树



## 红黑树和平衡二叉树的优略势

**红黑树的查询性能略微逊色于AVL树，因为他比avl树会稍微不平衡最多一层**，也就是说红黑树的查询性能只比相同内容的avl树最多多一次比较，但是，**红黑树在插入和删除上完爆avl树，avl树每次插入删除会进行大量的平衡度计算，而红黑树为了维持红黑性质所做的红黑变换和旋转的开销，相较于avl树为了维持平衡的开销要小得多**。

红黑树并不追求“完全平衡”——它只要求部分地达到平衡要求，降低了对旋转的要求，从而提高了性能。

总结：

AVL更平衡，结构上更加直观，时间效能针对读取而言更高；维护稍慢，空间开销较大。
红黑树，读取略逊于AVL，维护强于AVL，空间开销与AVL类似，内容极多时略优于AVL，维护优于AVL。

**如果搜索的次数远远大于插入和删除，那么选择AVL，如果搜索，插入删除次数几乎差不多，应该选择RB。**



## 排序算法时间、空间复杂度、稳定性

> https://blog.csdn.net/yushiyi6453/article/details/76407640

![image-20210330112627647](MyNotes.assets/image-20210330112627647.png)

### 快速排序

1. 算法思想
   通过一趟排序将待排记录分隔成独立的两部分，其中一部分记录的关键字均比另一部分的关键字小，则可分别对这两部分记录继续进行排序，以达到整个序列有序。

2. 实现原理
    2.1、设置两个变量 low、high，排序开始时：low=0，high=size-1。
    2.2、整个数组找基准正确位置，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面

  1. 默认数组的第一个数为基准数据，赋值给key，即key=array[low]。
  2. 因为默认数组的第一个数为基准，所以从后面开始向前搜索（high-–），找到第一个小于key的array[high]，就将 array[high] 赋给 array[low]，即 array[low] = array[high]。（循环条件是 array[high] >= key；结束时 array[high] < key）
  3. 此时从前面开始向后搜索（low++），找到第一个大于key的array[low]，就将 array[low] 赋给 array[high]，即 array[high] = array[low]。（循环条件是 array[low] <= key；结束时 array[low] > key）
  4. 循环 2-3 步骤，直到 low=high，该位置就是基准位置。
  5. 把基准数据赋给当前位置。

  2.3、第一趟找到的基准位置，作为下一趟的分界点。
  2.4、递归调用（recursive）分界点前和分界点后的子数组排序，重复2.2、2.3、2.4的步骤。
  2.5、最终就会得到排序好的数组。

```python
def QuickSort(self, arr, start, end):
    if len(arr) < 2:
        return arr
    low = start
    high = end
    pivot = arr[low]
    while low < high:
        while low < high and pivot < arr[high]:
            high -= 1
        if low < high:
            arr[low] = arr[high]
            low += 1
        while low < high and pivot > arr[low]:
            low += 1
        if low < high:
            arr[high] = arr[low]
            high -= 1
    arr[low] = pivot
    QucikSort(arr,0,low)
    QucikSort(arr,low+1,len(arr))
    return arr
```

### 堆排序

堆排序基本步骤
基本思想：

1.首先将待排序的数组构造成一个大根堆，此时，整个数组的最大值就是堆结构的顶端

2.将顶端的数与末尾的数交换，此时，末尾的数为最大值，剩余待排序数组个数为n-1

3.将剩余的n-1个数再构造成大根堆，再将顶端数与n-1位置的数交换，如此反复执行，便能得到有序数组

代码中主要的方法：

> https://blog.csdn.net/june_young_fan/article/details/82014081

(1)大根堆调整(max_heapify)：
**将堆的末端子节点作调整，使得子节点永远小于父节点，这是核心步骤，在建堆和堆排序都会用到。**
比较i的根节点和与其所对应i的孩子节点的值，当i根节点的值比左孩子节点的值要小的时候，就把i根节点和左孩子节点所对应的值交换，同理，就把i根节点和右孩子节点所对应的值交换。
然后再调用堆调整这个过程，可见这是一个递归的过程。

(2)建立大根堆(build_max_heap)：
将堆中所有的数据重新排序。建堆的过程其实就是不断做大根堆调整的过程，从(heapSize -2)//2处开始调整，一直调整到第一个根节点。

(3)堆排序(heap_sort)：
将根节点取出与最后一位做对调，并做最大堆调整的递归运算。堆排序是利用建堆和堆调整来进行的。
首先建堆，然后将堆的根节点选出与最后一个节点进行交换，然后将前面len(heap)-1个节点继续做堆调整，直到将所有的节点取出，对于有n个元素的一维数组我们只需要做n-1次操作。



### 归并排序

算法思想：

归：把数组不停的拆分为两组，分到最细之后再将两个有序数组进行排序的方法对其进行排序

并：将已有序的子序列合并，得到完全有序的序列

> https://blog.csdn.net/liguofang_527/article/details/105843451



## 查找算法

七大查找算法：顺序查找、二分查找、插值查找、斐波那契查找、

### 顺序查找

**说明：顺序查找适合于存储结构为顺序存储或链接存储的线性表。**

**基本思想：**顺序查找也称为线形查找，属于无序查找算法。从数据结构线形表的一端开始，顺序扫描，依次将扫描到的结点关键字与给定值k相比较，若相等则表示查找成功；若扫描结束仍没有找到关键字等于k的结点，表示查找失败。

### 二分查找

**说明：元素必须是有序的，如果是无序的则要先进行排序操作。**

**基本思想：**也称为是折半查找，属于有序查找算法。用给定值k先与中间结点的关键字比较，中间结点把线形表分成两个子表，若相等则查找成功；若不相等，再根据k与该中间结点关键字的比较结果确定下一步查找哪个子表，这样递归进行，直到查找到或查找结束发现表中没有这样的结点。

**复杂度分析：**最坏情况下，关键词比较次数为log2(n+1)，且**期望时间复杂度为O(log2n)**；

>  注：**折半查找的前提条件是需要有序表顺序存储，对于静态查找表，一次排序后不再变化，折半查找能得到不错的效率。但对于需要频繁执行插入或删除操作的数据集来说，维护有序的排序会带来不小的工作量，那就不建议使用。这种情况使用平衡二叉树或者红黑树效率会更高。**

### 插值查找

在介绍插值查找之前，首先考虑一个新问题，为什么上述算法一定要是折半，而不是折四分之一或者折更多呢？

折半查找这种查找方式，不是自适应的（也就是说是傻瓜式的）。二分查找中查找点计算如下：

$mid=(low+high)/2, 即mid=low+1/2*(high-low)$

通过类比，我们可以将查找的点改进为如下：

$mid=low+(key-a[low])/(a[high]-a[low])*(high-low)$，

也就是**将上述的比例参数1/2改进为自适应的，根据关键字在整个有序表中所处的位置，让mid值的变化更靠近关键字key**，这样也就间接地减少了比较次数。

### 斐波那契查找

斐波那契数列：1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89…….（从第三个数开始，后边每一个数都是前两个数的和）。然后我们会发现，**随着斐波那契数列的递增，前后两个数的比值会越来越接近0.618**，利用这个特性，我们就可以将黄金比例运用到查找技术中。

![img](MyNotes.assets/20150323100632467)

斐波那契查找与折半查找很相似，他是根据斐波那契序列的特点对有序表进行分割的。他**要求开始表中记录的个数为某个斐波那契数小1，及n=F(k)-1**;

 开始将k值与第F(k-1)位置的记录进行比较(及mid=low+F(k-1)-1),比较结果也分为三种

　　1）相等，mid位置的元素即为所求

　　2）>，low=mid+1,k-=2;

> 说明：low=mid+1说明待查找的元素在[mid+1,high]范围内，k-=2 说明范围[mid+1,high]内的元素个数为n-(F(k-1))= Fk-1-F(k-1)=Fk-F(k-1)-1=F(k-2)-1个，所以可以递归的应用斐波那契查找。

　　3）<，high=mid-1,k-=1。

> 说明：low=mid+1说明待查找的元素在[low,mid-1]范围内，k-=1 说明范围[low,mid-1]内的元素个数为F(k-1)-1个，所以可以递归的应用斐波那契查找。

### 树表查找

**基本思想：**二叉查找树是先对待查找的数据进行生成树，确保树的左分支的值小于右分支的值，然后在就行和每个节点的父节点比较大小，查找最适合的范围。 这个算法的查找效率很高，但是如果使用这种查找方法要首先创建树。 

主要类型有：二叉查找树、平衡二叉树、2-3查找树、红黑树、B树、B+树。

### 分块查找

分块查找又称索引顺序查找，它是顺序查找的一种改进方法。
**算法思想：**将n个数据元素"按块有序"划分为m块（m ≤ n）。每一块中的结点不必有序，但块与块之间必须"按块有序"；即第1块中任一元素的关键字都必须小于第2块中任一元素的关键字；而第2块中任一元素又都必须小于第3块中的任一元素，……

**算法流程：**

- 先选取各块中的最大关键字构成一个索引表；
- 查找分两个部分：先对索引表进行二分查找或顺序查找，以确定待查记录在哪一块中；然后，在已确定的块中用顺序法进行查找

### 哈希查找

**算法思想：**哈希的思路很简单，如果所有的键都是整数，那么就可以使用一个简单的无序数组来实现：将键作为索引，值即为其对应的值，这样就可以快速访问任意键的值。这是对于简单的键的情况，我们将其扩展到可以处理更加复杂的类型的键。

**算法流程：**

1）用给定的哈希函数构造哈希表；

2）根据选择的冲突处理方法解决地址冲突；

​	常见的解决冲突的方法：**拉链法和开放定址法**。

3）在哈希表的基础上执行哈希查找。

![image-20210330162249603](MyNotes.assets/image-20210330162249603.png)

## 二叉树遍历非递归实现

```C++
/*先序遍历
步骤为
1.根节点入栈
2.取栈顶元素出栈（出栈时访问）
3.先右子树后左子树入栈
*/
public List<Integer> preorderTraversal(TreeNode root) {
		if (root == null) {
			return null;
		}
		List<Integer> list = new ArrayList<Integer>();

		Stack<TreeNode> s = new Stack<TreeNode>();
		s.push(root);

		while (!s.isEmpty()) {
			
			TreeNode node = s.pop();
			list.add(node.val);
			
			if (node.right != null) {
				s.push(node.right);
			}
			
			if (node.left != null) {
				s.push(node.left);
			}
		}
		
		return list;
	}
```

```C++
/*
中序遍历
1.将根及其左子树都入栈
2.根据步骤1得到的栈，栈顶的右子树入栈（等同将右子树当成根节点再执行一遍操作）
3.出栈时访问
*/
public static List<Integer> inorderTraversal(TreeNode root) {
		if (root == null) {
			return null;
		}
		List<Integer> list = new ArrayList<Integer>();
		Stack<TreeNode> s = new Stack<TreeNode>();

		do {
			while (root != null) {
				s.push(root);
				root = root.left;
			}
			if (!s.isEmpty()) {
				TreeNode node = s.pop();
				list.add(node.val);
				root = node.right;
			}
		} while (!s.isEmpty() || root != null);

		return list;
	}
```

```C++
/*
后序遍历
顺序为左-右-根，可以当成根-右-左的反过程
也就是在先序遍历中先访问右子树再访问左子树
最后再借助一个栈来实现（将先序遍历中的访问改为入栈）
新栈中出栈时表示访问
*/
public static List<Integer> postorderTraversal(TreeNode root) {
		if (root == null) {
			return null;
		}
		List<Integer> list = new ArrayList<Integer>();

		Stack<TreeNode> s = new Stack<TreeNode>();
		
		s.push(root);
		
		while( !s.isEmpty() ) {
			TreeNode node = s.pop();
			if(node.left != null) {
				s.push(node.left);
			}
			
			if(node.right != null) {
				s.push(node.right);
			}
			
			list.add(0, node.val);
		}
		
		return list;
	}
```



## Trie（前缀树/字典树）

### 定义 

#### 1.Trie树 （特例结构树） 

   Trie树 ，又称 单词查找树、字典树 ，是一种 树形结构 ，是一种哈希树的变种，是一种用于快速检索的多叉树结构。典型应用是用于统计和排序大量的字符串（但不仅限于字符串），所以经常被搜索引擎系统用于文本词频统计。**它的优点是：最大限度地减少无谓的字符串比较，查询效率比哈希表高**

   **Trie的核心思想是空间换时间。利用字符串的公共前缀来降低查询时间的开销以达到提高效率的目的。**

   **Trie树也有它的缺点**,Trie树的内存消耗非常大.当然,或许用左儿子右兄弟的方法建树的话,可能会好点.

#### 2.  三个基本特性：　　

1）根节点不包含字符，除根节点外每一个节点都只包含一个字符。　　

2）从根节点到某一节点，路径上经过的字符连接起来，为该节点对应的字符串。　

3）每个节点的所有子节点包含的字符都不相同。

### 应用

#### 1. 字符串检索，词频统计，搜索引擎的热门查询

​    事先将已知的一些字符串（字典）的有关信息保存到trie树里，查找另外一些未知字符串是否出现过或者出现频率。

​    举例：

​    1）有一个1G大小的一个文件，里面每一行是一个词，词的大小不超过16字节，内存限制大小是1M。返回频数最高的100个词。

​    2）给出N 个单词组成的熟词表，以及一篇全用小写英文书写的文章，请你按最早出现的顺序写出所有不在熟词表中的生词。

​    3）给出一个词典，其中的单词为不良单词。单词均为小写字母。再给出一段文本，文本的每一行也由小写字母构成。判断文本中是否含有任何不良单词。例如，若rob是不良单词，那么文本problem含有不良单词。

​    4）1000万字符串，其中有些是重复的，需要把重复的全部去掉，保留没有重复的字符串

​    **5）寻找热门查询**：[搜索引擎](http://lib.csdn.net/base/searchengine)会通过日志文件把用户每次检索使用的所有检索串都记录下来，每个查询串的长度为1-255字节。假设目前有一千万个记录，这些查询串的重复读比较高，虽然总数是1千万，但是如果去除重复和，不超过3百万个。一个查询串的重复度越高，说明查询它的用户越多，也就越热门。请你统计最热门的10个查询串，要求使用的内存不能超过1G。

#### 2. 字符串最长公共前缀

​    Trie树利用多个字符串的公共前缀来节省存储空间，反之，当我们把大量字符串存储到一棵trie树上时，我们可以快速得到某些字符串的公共前缀。举例：

   1) 给出N 个小写英文字母串，以及Q 个询问，即询问某两个串的最长公共前缀的长度是多少.  解决方案：

​    首先对所有的串建立其对应的字母树。此时发现，对于两个串的最长公共前缀的长度即它们所在结点的公共祖先个数，于是，问题就转化为了离线  （Offline）的最近公共祖先（Least Common Ancestor，简称LCA）问题。

​    而最近公共祖先问题同样是一个经典问题，可以用下面几种方法：

​    \1. 利用并查集（Disjoint Set），可以采用采用经典的Tarjan [算法](http://lib.csdn.net/base/datastructure)；

​    \2. 求出字母树的欧拉序列（Euler Sequence ）后，就可以转为经典的最小值查询（Range Minimum Query，简称RMQ）问题了；

#### 3.  排序

​    Trie树是一棵多叉树，只要先序遍历整棵树，输出相应的字符串便是按字典序排序的结果。

​    举例： 给你N 个互不相同的仅由一个单词构成的英文名，让你将它们按字典序从小到大排序输出。

#### 4 作为其他数据结构和算法的辅助结构

​    如后缀树，AC自动机等。



# LeetCode

## 回溯算法

```python
results = []
def backtrack(路径，选择列表)：
	if 满足结束条件：
    	results.add(路径)
    	return
    for 选择 in 选择列表：
    	（剪枝）
    	做选择
        backtrack（路径，选择列表）
        撤销选择
```

```C++
// 51.N皇后
void putQueen(int row, int col, int n, vector<vector<int>> &occupied)
{
    int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int dy[] = {1, 0, -1, 1, -1, 1, 0, -1};
    for (int i = 0; i < 8; i++)
    {
        int pos_x = col;
        int pos_y = row;
        for (int j = 0; j < n; j++)
        {
            pos_y += dy[i];
            pos_x += dx[i];
            if (pos_x >= 0 && pos_y >= 0 && pos_x < n && pos_y < n)
            {
                occupied[pos_y][pos_x] = 1;
            }
            else
            {
                break;
            }
        }
    }
}
void backtrackQueen(int row, int n, vector<vector<string>> &ans, vector<vector<int>> &occupied, vector<string> &place)
{
    if (row == n - 1)
    {
        ans.push_back(place);
        return;
    }
    for (int i = 0; i < n; ++i)
    {
        if (occupied[row + 1][i] == 1)
        {
            continue;
        }
        place[row + 1][i] = 'Q';
        vector<vector<int>> tmp(occupied);
        putQueen(row + 1, i, n, occupied);
        backtrackQueen(row + 1, n, ans, occupied, place);
        place[row + 1][i] = '.';
        occupied = tmp;
    }
}
vector<vector<string>> solveNQueens(int n)
{
    vector<vector<string>> ans;
    vector<vector<int>> occupied;
    vector<string> place;
    for (int i = 0; i < n; ++i)
    {
        occupied.push_back(vector<int>(n));
        for (int j = 0; j < n; ++j)
        {
            occupied[i].push_back(0);
        }
        place.push_back(string(n, '.'));
    }
    backtrackQueen(-1, n, ans, occupied, place);
    return ans;
}
```

## 动态规划

### 01背包问题

当 i > 0 时`dp[i][j]`有两种情况：

1. 不装入第i件物品，即`dp[i−1][j]`；
2. 装入第i件物品（前提是能装下），即`dp[i−1][j−w[i]] + v[i]`。

状态转移方程为

```text
dp[i][j] = max(dp[i−1][j], dp[i−1][j−w[i]]+v[i]) // j >= w[i]
```

```text
// 01背包问题伪代码(空间优化版)
dp[0,...,W] = 0
for i = 1,...,N
    for j = W,...,w[i] // 必须逆向枚举!!!
        dp[j] = max(dp[j], dp[j−w[i]]+v[i])
```

### 完全背包问题

状态转移方程为

```text
dp[i][j] = max(dp[i−1][j], dp[i][j−w[i]]+v[i]) // j >= w[i]
```

这个状态转移方程与01背包问题唯一不同就是`max`第二项不是`dp[i-1]`而是`dp[i]`。

```text
// 完全背包问题思路一伪代码(空间优化版)
dp[0,...,W] = 0
for i = 1,...,N
    for j = w[i],...,W // 必须正向枚举!!!
        dp[j] = max(dp[j], dp[j−w[i]]+v[i])
```

## 排序中的TopK问题

- 思路一：对整个数据进行排序，然后选出前K个，可以选择快速排序、堆排序和归并排序，其时间复杂度都可以达到O(NlogN)
- 思路二：不进行全排列，只对最大的K个排列。如利用冒泡法，每次冒泡取出最大的，进行K次，即可求得Top K.时间复杂度是O(n*k).
- 思路三：利用数据池思想，首先 维护一个大小为K的数据池，将前K个数放进去，然后将原数据中剩余的N-K个数，依次与数据池中的数据做比较，遇到比当前数小的，则替换。此时的算法时间复杂度为O(N*K)

- 思路四：利用快速排序的分划函数找到分划位置K，即第K大的数，则其左侧即为所求。

## 快速排序的优化

#### [快排基准的选择](https://blog.csdn.net/qq_38289815/article/details/82718428#快排基准的选择)

​     固定基准

​     随机基准

​     三数取中

#### [快速排序的优化](https://blog.csdn.net/qq_38289815/article/details/82718428#快速排序的优化)

优化1：序列长度达到一定大小时，使用插入排序

优化2：尾递归优化

优化3：聚集元素

优化4：多线程处理快排

#### [尾递归][https://blog.csdn.net/Vermont_/article/details/84557065]

在传统的递归中，典型的模型是首先执行递归调用，然后获取递归调用的返回值并计算结果。以这种方式，在每次递归调用返回之前，您不会得到计算结果。传统地递归过程就是函数调用，涉及返回地址、函数参数、寄存器值等压栈（在x86-64上通常用寄存器保存函数参数），这样做的缺点有二：

- 效率低，占内存

  如果递归链过长，可能会stack overflow

尾递归的原理：

在尾递归中，首先执行计算，然后执行递归调用，将当前步骤的结果传递给下一个递归步骤。这导致最后一个语句采用的形式(return (recursive-function params))。基本上，任何给定递归步骤的返回值与下一个递归调用的返回值相同。

## 二分(边界)查找算法

> https://segmentfault.com/a/1190000016825704

**两大基本原则**：

1. **每次都要缩减搜索区间**
2. **每次缩减不能排除潜在答案**

> 以下仅供参考，并不能套用所有题型

| 查找方式     | 循环条件        | 左侧更新         | 右侧更新          | 中间点位置               | 返回值       |
| :----------- | :-------------- | :--------------- | :---------------- | :----------------------- | :----------- |
| 标准二分查找 | `left <= right` | `left = mid + 1` | `right = mid - 1` | `(left + right) / 2`     | `-1 / mid`   |
| 二分找左边界 | `left < right`  | `left = mid`     | `right = mid - 1` | `(left + right) / 2 + 1` | `-1 / right` |
| 二分找右边界 | `left < right`  | `left = mid + 1` | `right = mid`     | `(left + right) / 2`     | `-1 / left`  |

`返回值范围为[0,size-1]，插入时要注意边界判断。`

二分法的应有**标准二分查找、二分查找左边界、二分查找右边界**、二分查找左右边界、二分查找极值等

它们基本上是**循环条件**，**判断条件**，**边界更新方法**的不同组合。



## 位运算符

| **运算符** | **作用**     | **示例**                                   |
| ---------- | ------------ | ------------------------------------------ |
| **&**      | **按位与**   | **两个操作数同时为1结果为1**               |
| **\|**     | **按位或**   | **两个操作数只要有一个为1，结果就为1**     |
| **~**      | **按位非**   | **操作数为1，结果为0；操作数为0，结果为1** |
| **^**      | **按位异或** | **两个操作数相同，结果为0；不相同结果为1** |
| **<<**     | **左移**     | **右侧空位补0**                            |
| **>>**     | **右移**     | **左侧空位补符号位**                       |

```C++
// 190. 颠倒二进制位
class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        uint32_t rev = 0;
        for (int i = 0; i < 32 && n > 0; ++i) {
            rev |= (n & 1) << (31 - i);
            n >>= 1;
        }
        return rev;
    }
};
```



# Computer Graphics

## 几何题

1.两圆相交，只有2交点A、B，过A点做线段CAD，CA、AD为两圆的弦，问什么情况下CAD最长，并证明。*提示圆心角和圆周角的两倍关系。*

![img](MyNotes.assets/20140118155413296)

如图分别连接BC，BD。当CAD不断的变化的时候，可以得到个无数个三角形CBD，这些三角形的夹BCA和角BDA都是相同的，分别是BA对应的圆心角的一半。

则这些三角形都是**相似**的。

那么当BD最大的时候，也就是三角形最大的时候，也就是CAD最长的时候。

BD最大的时候就是BD为圆的直径，则BAD为直角，同理，BAC也是直角=>BA 垂直CAD。



2.平面上N个点，每两个点都确定一条直线， 求出斜率最大的那条直线所通过的两个点（斜率不存在的情况不考虑）时间效率越高越好。

程序的基本步骤就是：

- 1.把N个点按x坐标排序。
- 2.遍历，求相邻的两个点的斜率，找最大值。

时间复杂度Nlog(N)

**如下图所示，排序后的斜率最大值一定出现在相邻点之间**。

![image-20210423152610864](MyNotes.assets/image-20210423152610864.png)

## 直线绘制算法

- 数值微分法（DDA）

  基本流程：

  - **当*x*每递增1，*y*递增*k*(即直线斜率)；**
  - 注意上述分析的算法仅适用于|*k*| ≤1的情形。在这种情况下，*x*每增加1,*y*最多增加1。
  - 当 |*k*| >1时，必须把*x*，*y*位置互换
  - 按照直线从（x0，y0）到（x1，y1）的方向不同，分为8个象限。对于方向在第1a象限内的直线而言， Dx=1， Dy=k。对于方向在第1b象限内的直线而言，取值Dy=1， Dx=1/k。

```C++
void LineDDA(int x0,int y0,int x1,int y1/*,int color*/)
{
    int x, dy, dx, y;
    float m;
	dx=x1-x0;
    dy=y1-y0;
    
	if(abs(dx)>=abs(dy))
		m=abs(dx);          //x为计长方向
	else
		m=abs(dy);          //y为计长方向
    y=y0;
    
    glColor3f (1.0f, 1.0f, 0.0f);
    glPointSize(1);

    for(x=x0;x<=x1; x++)
    {
        glBegin (GL_POINTS);
        glVertex2i (x, (int)(y+0.5));
        glEnd ();
        y+=m;
    }
}
```

- 中点画线法

  当前像素点为p(xp, yp) ，下一个象素点为P1或P2。设M=(xp+1, yp+0.5)，为p1与p2的中点，Q为理想直线与x=xp+1垂线的交点。将Q与M的y坐标进行比较。

  如下图所示：

  ![img](MyNotes.assets/702782-20160111162439835-1084293073.png)

  当M在Q的下方，则P2应为下一个象素点；

  当M在Q的上方，应取P1为下一点。

- Bresenham算法

  这种画线算法的思想和中点画线的一致，只是在判断取哪个点时，不是看它位于中点的上边还是下边，而是将这俩个点到直线上那个点的距离相减，判断其正负，如果下边的点到直线实际点距离远则的d1-d2>=0那么取上边的点y1+1。

  ![img](MyNotes.assets/702782-20160111164818991-1732572784.png)

## 区域填充算法

1.种子填充算法/漫水法

种子填充算法是从多边形区域内部的一点开始，由此出发找到区域内的所有像素。种子填充算法采用的边界定义是区域边界上所有像素具有某个特定的颜色值，区域内部所有像素均不取这一特定颜色，而边界外的像素则可具有与边界相同的颜色值。
**其实就是利用DFS或BFS算法进行遍历。**

```C++
//四连通漫水法伪代码
void FloodFill (x, y, newcolor, boundaryColor)
{
     Stack stack;
     stack.Push(Pixel(x, y));	//把种子像素(x,y)推入栈中
     while (! stack.Empty())  	//当栈不空时循环执行以下代码
     {
         pixel=stack.Pop();	//从栈顶弹出一个像素
            //当处理内定义区域时，用if (pixel.Color !=newcolor)判断即可
         if (pixel.Color !=newcolor && pixel.Color !=boundaryColor)
         {
             xx=pixel.x; yy=pixel.y;
            setpixel( xx,  yy,  newcolor, boundaryColor);
            stack. Push ( Pixel (xx-1, yy  )) ;
            stack. Push ( Pixel( xx,  yy+1)); 
            stack. Push ( Pixel (xx+1, yy  )); 
            stack.Push ( Pixel(xx, yy-1));
         }
    }
}
```

2.扫描线种子填充算法

基本过程如下：当给定种子点(x, y)时，首先分别向左和向右两个方向填充种子点所在扫描线上的位于给定区域的一个区段，同时记下这个区段的范围[xLeft, xRight]，然后确定与这一区段相连通的上、下两条扫描线上位于给定区域内的区段，并依次保存下来。反复这个过程，直到填充结束。

> 上面是一个点一个点进行涂色，这种方法是一条线一条线（区间）进行涂色。

## 图像颜色空间你熟悉几种（颜色模型）

> https://zhuanlan.zhihu.com/p/28741691

1）RGB颜色空间

RGB（红绿蓝）是依据人眼识别的颜色定义出的空间，可表示大部分颜色。但在科学研究一般不采用RGB颜色空间，因为它的细节难以进行数字化的调整。它将色调，亮度，饱和度三个量放在一起表示，很难分开。

2）CMY/CMYK颜色空间

CMY是工业印刷采用的颜色空间。它与RGB对应。简单的类比RGB来源于是物体发光，而CMY是依据反射光得到的。具体应用如打印机：一般采用四色墨盒，即CMY加黑色墨盒。

3）HSV/HSB颜色空间

HSV颜色空间是为了更好的数字化处理颜色而提出来的。有许多种HSX颜色空间，其中的X可能是V,也可能是I，依据具体使用而X含义不同。H是色调，S是饱和度，I是强度。HSB（Hue, Saturation, Brightness）颜色模型，这个颜色模型和HSL颜色模型同样都是用户台式机图形程序的颜色表示， 用六角形锥体表示自己的颜色模型。

![img](MyNotes.assets/v2-b77070bbb0a41aaa1ad5339422cd8b18_720w.jpg)

4）HSI/HSL颜色空间

HSI颜色空间是为了更好的数字化处理颜色而提出来的。有许多种HSX颜色空间，其中的X可能是V,也可能是I，依据具体使用而X含义不同。H是色调，S是饱和度，I是强度。HSL（Hue, Saturation, Lightness）颜色模型，这个颜色模型都是用户台式机图形程序的颜色表示， 用六角形锥体表示自己的颜色模型。

![img](MyNotes.assets/v2-151a5fe079d04c9f186a9e61f7ef3a02_720w.jpg)

5）Lab颜色空间

[Lab颜色模型](https://link.zhihu.com/?target=http%3A//baike.baidu.com/item/Lab%E9%A2%9C%E8%89%B2%E6%A8%A1%E5%9E%8B)是由CIE（[国际照明委员会](https://link.zhihu.com/?target=http%3A//baike.baidu.com/item/%E5%9B%BD%E9%99%85%E7%85%A7%E6%98%8E%E5%A7%94%E5%91%98%E4%BC%9A)）制定的一种色彩模式。自然界中任何一点色都可以在Lab空间中表达出来，它的[色彩空间](https://link.zhihu.com/?target=http%3A//baike.baidu.com/item/%E8%89%B2%E5%BD%A9%E7%A9%BA%E9%97%B4)比RGB空间还要大。另外，这种模式是以数字化方式来描述人的视觉感应， 与设备无关，所以它弥补了RGB和CMYK模式必须依赖于设备色彩特性的不足。

![img](MyNotes.assets/v2-62dd404d83b9fd6588a19478af840e39_720w.jpg)

6）YUV/YCbCr颜色空间

YUV是通过亮度-色差来描述颜色的颜色空间。亮度信号经常被称作Y，色度信号是由两个互相独立的信号组成。

## 在计算机图形学中如何进行平移、旋转、缩放？

> https://blog.csdn.net/a3631568/article/details/53637473?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-5.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7Edefault-5.control

- 二维**平移**矩阵：

![img](MyNotes.assets/20161214134139727)

- 二维**旋转**矩阵：绕坐标系原点的而为旋转变换方程可以表示成矩阵形式：

![img](MyNotes.assets/20161214134149521)

x’,y’表示变换后的坐标。

如果要基于某个基准点旋转，则：

1.平移对象使基准点位置移动到坐标原点

2.绕坐标原点旋转

3.平移对象使基准点回到原始位置

矩阵公式则是：

![img](MyNotes.assets/20161214134220415)

- 二维**缩放**矩阵：（也是基于原点）

![img](MyNotes.assets/20161214134229490)


如何要基于某个固定点进行缩放，那么就需要二维复合变换矩阵。

基于某个固定点缩放原理：其实就是先平移然后再缩放，最后再返回到原点。

于是有：

![img](MyNotes.assets/20161214134235587)

上述矩阵就是基于某个固定点的缩放。 

必须注意的是：复合矩阵求值的顺序一定不能交换，除非某些情况譬如连续两次旋转或者连续两次平移等等。

为什么需要用复合矩阵相乘？而不直接一次一次的变换得到结果呢？

因为**直接进行复合矩阵的计算会减少乘法和加法次数。**



## 如何判断空间中一点是否在球体内？

从待检测点发射一条射线。当射线与三维物体的交点为奇数个，则在物体内部。为偶数个则在物体外部。这个原理称之为**奇偶规则**。



## 3D图形渲染管线

**什么是渲染（Rendering）**

渲染简单的理解可能可以是这样：就是将三维物体或三维场景的描述转化为一幅二维图像，生成的二维图像能很好的反应三维物体或三维场景

![img](MyNotes.assets/20170111161204833)

![img](MyNotes.assets/20190130163858595.png)

## 深度测试

OpenGL存储它的所有深度信息于一个Z缓冲(Z-buffer)中，也被称为深度缓冲(Depth Buffer)。GLFW会自动为你生成这样一个缓冲（就像它也有一个颜色缓冲来存储输出图像的颜色）。深度值存储在每个片段里面（作为片段的z值），当片段想要输出它的颜色时，**OpenGL会将它的深度值和z缓冲进行比较，如果当前的片段在其它片段之后，它将会被丢弃，否则将会覆盖。这个过程称为深度测试(Depth Testing)**，它是由OpenGL自动完成的。

![img](MyNotes.assets/v2-1b078f9ac5beee8ec4fc825582626e4c_720w.jpg)

上图结果是已开启深度测试的状态。

下图未开启Z轴缓存，会出现重叠区域被清理

![img](MyNotes.assets/v2-e05f2ebfbfcb057ee6dea345c53dd4a7_720w.jpg)





# Other

> - [ ] https://blog.csdn.net/qq_27262727/article/details/105108229?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_baidulandingword-0&spm=1001.2101.3001.4242
>
> - [ ] https://blog.csdn.net/yzt629/article/details/102475485?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-8.control&dist_request_id=1332041.8864.16192241318421465&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-8.control
>
> - [ ] https://blog.csdn.net/x3464/article/details/115019319?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-11.control&dist_request_id=1332041.8864.16192241318421465&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-11.control

## 设计模式、单例模式

这些模式可以分为三大类：创建型模式（Creational Patterns）、结构型模式（Structural Patterns）、行为型模式（Behavioral Patterns）

**创建型模式（五种）：** 

- **工厂模式**：定义一个创建对象的接口，让其子类自己决定实例化哪一个工厂类，工厂模式使其创建过程延迟到子类进行。
- **抽象工厂模式**：提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。
- **单例模式**：保证一个类仅有一个实例，并提供一个访问它的全局访问点。
- **建造者模式**：将一个复杂的构建与其表示相分离，使得同样的构建过程可以创建不同的表示。
- 原型模式：用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。

**结构型模式（七种）：**

- **适配器模式**：将一个类的接口转换成客户希望的另外一个接口。适配器模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
- 桥接模式：将抽象部分与实现部分分离，使它们都可以独立的变化。
- 过滤器模式：这种模式允许开发人员使用不同的标准来过滤一组对象，通过逻辑运算以解耦的方式把它们连接起来
- 组合模式：将对象组合成树形结构以表示"部分-整体"的层次结构。组合模式使得用户对单个对象和组合对象的使用具有一致性。
- 装饰器模式：动态地给一个对象添加一些额外的职责。就增加功能来说，装饰器模式相比生成子类更为灵活。
- 外观模式：为子系统中的一组接口提供一个一致的界面，外观模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。
- **享元模式**：运用共享技术有效地支持大量细粒度的对象。
- **代理模式**：为其他对象提供一种代理以控制对这个对象的访问。

**行为型模式（十一种）：**

- 责任链模式：避免请求发送者与接收者耦合在一起，让多个对象都有可能接收请求，将这些对象连接成一条链，并且沿着这条链传递请求，直到有对象处理它为止。
- 命令模式：将一个请求封装成一个对象，从而使您可以用不同的请求对客户进行参数化。
- 解释器模式：给定一个语言，定义它的文法表示，并定义一个解释器，这个解释器使用该标识来解释语言中的句子。
- 迭代器模式：提供一种方法顺序访问一个聚合对象中各个元素, 而又无须暴露该对象的内部表示。
- 中介者模式：用一个中介对象来封装一系列的对象交互，中介者使各对象不需要显式地相互引用，从而使其耦合松散，而且可以独立地改变它们之间的交互。
- 备忘录模式：在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。
- **观察者模式**：指多个对象间存在一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。
- 状态模式：定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。
- 空对象模式：在空对象模式中，我们创建一个指定各种要执行的操作的抽象类和扩展该类的实体类，还创建一个未对该类做任何实现的空对象类，该空对象类将无缝地使用在需要检查空值的地方。
- **策略模式**：定义一系列的算法,把它们一个个封装起来, 并且使它们可相互替换。
- 模板模式：定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
- 访问者模式：主要将数据结构与数据操作分离。



## 海量数据处理

> https://blog.csdn.net/v_july_v/article/details/7382693

**大而化小，各个击破，缩小规模，逐个解决**

set/map/multiset/multimap都是基于RB-tree之上，所以有自动排序功能，而hash_set/hash_map/hash_multiset/hash_multimap都是基于hashtable之上，所以不含有自动排序功能，至于加个前缀multi_无非就是允许键值重复而已。

![img](MyNotes.assets/20131017120243656)

### Bloom Filter



### 外排序

> https://www.bilibili.com/video/BV1gJ411u7JF?from=search&seid=16629045435250066559



## 高并发

> https://blog.51cto.com/zjunzz/2299512



## 线程安全

### 1、悲观锁

解决线程安全的思路很多，可以从“悲观锁”的方向开始讨论。**悲观锁，也就是在修改数据的时候，采用锁定状态，排斥外部请求的修改。遇到加锁的状态，就必须等待。**虽然上述的方案的确解决了线程安全的问题，但是，别忘记，我们的场景是“高并发”。也就是说，会很多这样的修改请求，**每个请求都需要等待“锁”，某些线程可能永远都没有机会抢到这个“锁”，这种请求就会死在那里**。同时，这种请求会很多，瞬间增大系统的平均响应时间，结果是可用连接数被耗尽,系统陷入异常。

悲观锁：通过数据库的forupdate字段实现加锁。for update要放到mysql的事务中，即begin和commit中，否者则不起作用。

### 2、FIFO队列思路

那好，那么我们稍微修改一下上面的场景，我们直接**将请求放入队列中的，采用FIFO（First Input First Output，先进先出）**，这样的话，我们就不会导致某些请求永远获取不到锁。看到这里，是不是有点**强行将多线程变成单线程**的感觉哈。

然后，我们现在解决了锁的问题，全部请求采用“先进先出”的队列方式来处理。那么新的问题来了，高并发的场景下，因为请求很多，很可能一瞬间将队列内存“撑爆”，然后系统又陷入到了异常状态。或者设计一个极大的内存队列，也是一种方案，但是，系统处理完一个队列内请求的速度根本无法和疯狂涌 入队列中的数目相比。也就是说，队列内的请求会越积累越多，最终Web系统平均响应时候还是会大幅下降，系统还是陷入异常。

### 3、乐观锁思路

这个时候，我们就可以讨论一下“乐观锁”的思路了。**乐观锁，是相对于“悲观锁”采用更为宽松的加锁机制，大都是采用带版本号（Version）更新。**实现就是，这个数据所有请求都有资格去修改，但会获得一个该数据的版本号，**只有版本号符合的才能更新成功**，其他的返回抢购失败。这样的话，我们就不需要考虑队列的问题，不过，它**会增大CPU的计算开销**。但是，综合来说，这是一个比较好的解决方案。有很多软件和服务都“乐观锁”功能的支持，例如Redis中的watch就是其中之一。通过这个实现，我们保证了数据的安全。

```text
举例：

修改版本号，version。

假设数据库中帐户信息表中有一个 version字段，当前值为 1 ；而当前帐户余额字段（ balance ）为 $100 。

1 操作员 A 此时将其读出（ version=1 ），并从其帐户余额中扣除 $50（ $100-$50 ）。

2 在操作员 A 操作的过程中，操作员 B 也读入此用户信息（ version=1 ），并 从其帐户余额中扣除 $20 （ $100-$20 ）。

3 操作员 A完成了修改工作，将数据版本号加一（ version=2 ），连同帐户扣 除后余额（ balance=$50 ），提交至数据库更新，此时由于提交数据版本大于数据库记录当前版本，数据被更新，数据库记录 version 更新为 2 。

4 操作员 B 完成了操作，也将版本号加一（ version=2 ）试图向数据库提交数据（ balance=$80 ），但此时比对数据库记录版本时发现，操作员 B提交的 数据版本号为 2 ，数据库记录当前版本也为 2 ，不满足“ 提交版本必须大于记 录当前版本才能执行更新 “ 的乐观锁策略，因此，操作员 B 的提交被驳回。 这样，就避免了操作员 B 用基于version=1 的旧数据修改的结果覆盖操作员 A 的操作结果的可能。
```


从上面的例子可以看出，乐观锁机制避免了长事务中的数据库加锁开销（操作员 A和操作员 B 操作过程中，都没有对数据库数据加锁），大大提升了大并发量下的系统整体性能表现。







---

# END





