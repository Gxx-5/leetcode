# LeetCode
from typing import List
import math


class Solution:
    # 剑指 Offer 63 股票的最大利润
    def maxProfit(self, prices: List[int]) -> int:
        min_p = max(prices)
        ans = 0
        for p in prices:
            if p < min_p:
                min_p = p
            elif ans < p - min_p:
                ans = p - min_p
        return ans

    # 剑指 Offer 51. 数组中的逆序对
    def sortInsert(self, n: int, seg: List[int]) -> int:
        size = len(seg)
        low = 0
        high = size - 1
        mid = int((low + high) / 2)
        while low <= high:
            mid = int((low + high) / 2)
            if seg[mid] == n:
                while seg[mid] == n:
                    mid -= 1
                    if mid == -1:
                        break
                seg.insert(mid + 1, n)
                return mid + 1
            elif seg[mid] < n:
                low = mid + 1
            else:
                high = mid - 1

        if n > seg[mid]:
            # 二分查找的mid指针永远指向表内元素，但带查找元素比表中任何元素都大时，需要加1
            seg.insert(mid + 1, n)
            return mid + 1
        seg.insert(mid, n)
        return mid

    def reversePairs(self, nums: List[int]) -> int:
        size = len(nums)
        if size < 2:
            return 0
        ans = 0
        seg = [nums[size - 1]]
        for i in range(size - 2, -1, -1):
            ans += self.sortInsert(nums[i], seg)
            print(ans)
        return ans

    # 面试题59 - I. 滑动窗口的最大值
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        ans = []
        i = 0
        while i + k < len(nums):
            ans.append(max(nums[i:i + k]))
        return ans

    # 面试题 17.21. 直方图的水量
    def trap(self, height: List[int]) -> int:
        if len(height) < 3:
            return 0
        stack = []
        ans = 0
        for h in height:
            if len(stack) == 0:
                stack.append(h)
            elif h <= stack[len(stack) - 1]:
                stack.append(h)
            else:
                tmp = []
                while len(stack) != 0 and h > stack[len(stack) - 1]:
                    tmp.append(stack.pop())
                if len(stack) != 0:
                    tmp.append(stack.pop())
                hei = min(tmp[len(tmp) - 1], h)
                for i in tmp[0:-1]:
                    ans += hei - i
                if tmp[len(tmp) - 1] >= h:
                    stack.append(tmp[len(tmp) - 1])
                    for i in range(len(tmp) - 1):
                        stack.append(h)
                stack.append(h)
        return ans

    def numRabbits(self, answers: List[int]) -> int:
        count = dict()
        for n in answers:
            if count.get(n + 1) is None:
                count[n + 1] = 1
            else:
                count[n + 1] += 1
        ans = 0
        for c in count:
            n = count.get(c)
            ans += math.ceil(n / c) * c
        return ans


if __name__ == '__main__':
    so = Solution()
    print("ans: ", so.trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))
